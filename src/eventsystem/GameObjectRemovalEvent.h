#pragma once

#include "Event.h"
#include "GameObject.h"

class GameObjectRemovalEvent : public Event
{
public:
	GameObjectRemovalEvent(GameObject* g);
	~GameObjectRemovalEvent();

	GameObject* gameobject;
};

