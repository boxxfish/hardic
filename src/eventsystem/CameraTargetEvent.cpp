#include "stdafx.h"
#include "CameraTargetEvent.h"


CameraTargetEvent::CameraTargetEvent(Transform* target, bool instant) : Event()
{
	this->target = target;
	this->instant = instant;
}


CameraTargetEvent::~CameraTargetEvent()
{
}
