#include "stdafx.h"
#include "EventListener.h"


EventListener::EventListener()
	:evtGenerated(false)
{
}


EventListener::~EventListener()
{
}

bool EventListener::ApplyBroadcast(Event* evt) {
	this->HandleEvent(evt);
	bool evtGen = this->evtGenerated;
	this->evtGenerated = false;
	return evtGen;
}

Event * EventListener::GetReturnEvt() {
	return this->returnEvt;
}

void EventListener::SendEvent(Event* evt) {
	this->evtGenerated = true;
	this->returnEvt = evt;
}
