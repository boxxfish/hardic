#pragma once

#include <eventsystem/Event.h>
#include <engine/Transform.h>

class CameraTargetEvent : public Event
{
public:
	CameraTargetEvent(Transform* target, bool instant = false);
	~CameraTargetEvent();

	Transform* target;
	bool instant;
};

