#pragma once

#include "Event.h"
#include "GameObject.h"

class GameObjectCreationEvent : public Event
{
public:
	GameObjectCreationEvent(GameObject* g);
	GameObjectCreationEvent(GameObject* g, std::string parentName);
	GameObjectCreationEvent(GameObject* g, GameObject* parent);
	~GameObjectCreationEvent();

	GameObject* gameobject;
	std::string parentName;
	GameObject* parent;
};

