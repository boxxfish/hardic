#pragma once

#include <vector>

#include "EventListener.h"

class EventQueue
{
public:
	EventQueue();
	~EventQueue();

	void Broadcast(Event* evt);

	void RegisterListener(EventListener* listener);
private:
	std::vector<EventListener*> listeners;
};