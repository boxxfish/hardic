#include "stdafx.h"
#include "EventQueue.h"


EventQueue::EventQueue()
{
}


EventQueue::~EventQueue()
{
}

void EventQueue::Broadcast(Event* evt) {
	for (int i = 0;i < this->listeners.size(); i++) {
		bool evtGenerated = this->listeners[i]->ApplyBroadcast(evt);
		if (evtGenerated)
			this->Broadcast(this->listeners[i]->GetReturnEvt());
	}
	delete evt;
}

void EventQueue::RegisterListener(EventListener* listener) {
	this->listeners.push_back(listener);
}
