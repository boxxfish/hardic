#pragma once

#include "EventQueue.h"
#include "GameObject.h"

class SceneEventListener : public EventListener
{
public:
	SceneEventListener();
	~SceneEventListener();

	void HandleEvent(Event* evt);
	std::vector<GameObject*>* gameObjects;
};

