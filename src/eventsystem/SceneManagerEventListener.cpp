#include "stdafx.h"
#include "SceneManagerEventListener.h"


SceneManagerEventListener::SceneManagerEventListener() : EventListener()
{
}


SceneManagerEventListener::~SceneManagerEventListener()
{
}

void SceneManagerEventListener::HandleEvent(Event* evt) {
	if (GameObjectCreationEvent* crEvt = dynamic_cast<GameObjectCreationEvent*>(evt)) {
		if(crEvt->parentName != "")
			this->activeScene->AddGameObject(crEvt->gameobject, crEvt->parentName);
		else if (crEvt->gameobject != NULL)
			this->activeScene->AddGameObject(crEvt->gameobject, crEvt->parent);
		else
			this->activeScene->AddGameObject(crEvt->gameobject);
	}
	else if (GameObjectRemovalEvent* rmEvt = dynamic_cast<GameObjectRemovalEvent*>(evt))
		this->activeScene->RemoveGameObject(rmEvt->gameobject);
}
