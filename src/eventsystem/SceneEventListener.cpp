#include "stdafx.h"
#include "SceneEventListener.h"


SceneEventListener::SceneEventListener() : EventListener()
{
}


SceneEventListener::~SceneEventListener()
{
}

void SceneEventListener::HandleEvent(Event* evt) {
	//Send event to listening gameobjects
	for (int i = 0; i < this->gameObjects->size(); i++) {
		if ((*this->gameObjects)[i]->IsReceivingEvents())
			(*this->gameObjects)[i]->HandleEvent(evt);
	}
}
