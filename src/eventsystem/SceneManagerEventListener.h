#pragma once

#include "EventListener.h"
#include <eventsystem/GameObjectCreationEvent.h>
#include <eventsystem/GameObjectRemovalEvent.h>
#include "Scene.h"

class SceneManagerEventListener : public EventListener
{
public:
	SceneManagerEventListener();
	~SceneManagerEventListener();

	void HandleEvent(Event* evt);
	Scene* activeScene;
};

