#include "stdafx.h"
#include "GameObjectCreationEvent.h"


GameObjectCreationEvent::GameObjectCreationEvent(GameObject* g) : Event(), gameobject(g)
{
}

GameObjectCreationEvent::GameObjectCreationEvent(GameObject * g, std::string parentName) : Event(), gameobject(g), parentName(parentName)
{
}

GameObjectCreationEvent::GameObjectCreationEvent(GameObject * g, GameObject * parent) : Event(), gameobject(g), parent(parent)
{
}


GameObjectCreationEvent::~GameObjectCreationEvent()
{
}
