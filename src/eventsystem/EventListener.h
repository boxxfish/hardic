#pragma once

#include "Event.h"

class EventListener
{
public:
	EventListener();
	~EventListener();

	virtual void HandleEvent(Event* evt) = 0;

	bool ApplyBroadcast(Event* evt);
	Event* GetReturnEvt();
protected:
	void SendEvent(Event* evt);
private:
	Event* returnEvt;
	bool evtGenerated;
};

