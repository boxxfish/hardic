#pragma once

#include <vector>

#include <Engine/Mesh.h>
#include <Engine/Renderer.h>

class Model
{
public:
	struct ModelMesh {
		std::string tex;
		std::vector<float> verts;
		std::vector<float> texCoords;
		std::vector<float> normals;
		std::vector<float> GetCoords() {
			std::vector<float> result;
			int coordCount = verts.size() / 3;
			for (int i = 0; i < coordCount; i++) {
				//Verts
				result.push_back(this->verts[i * 3]);
				result.push_back(this->verts[i * 3 + 1]);
				result.push_back(this->verts[i * 3 + 2]);
				//TexCoords
				result.push_back(this->texCoords[i * 2]);
				result.push_back(this->texCoords[i * 2 + 1]);
				//Normals
				result.push_back(this->normals[i * 3]);
				result.push_back(this->normals[i * 3 + 1]);
				result.push_back(this->normals[i * 3 + 2]);
			}
			return result;
		}
	};

	Model();
	~Model();

	std::vector<ModelMesh> meshes;
};

