#pragma once

#include <vector>
#include <iostream>

#include "Importer.h"
#include "Tileset.h"
#include <Renderer.h>

class LevelMap
{
public:
	LevelMap();
	LevelMap(std::vector<char> data, Resources* resources);
	~LevelMap();

	unsigned int width, height;
	unsigned int numTileLayers;
	std::vector<std::vector<std::vector<unsigned int>>> tileLayer;
	std::vector<std::vector<unsigned int>> objectLayer;
	Tileset tileset;
};