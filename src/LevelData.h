#pragma once

#include <Tileset.h>
#include <LevelMap.h>
#include <helpers/ObjectLayer.h>

class LevelData
{
public:
	LevelData();
	LevelData(LevelMap map);
	~LevelData();

	ObjectLayer<unsigned int> tileLayer;
	ObjectLayer<unsigned int> objectLayer;
	LevelMap map;
};

