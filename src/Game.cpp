#include "stdafx.h"
#include "Game.h"


Game::Game() {
	//Set AppSettings
	this->services.appSettings.windowWidth = 1024;
	this->services.appSettings.windowHeight = 576;

	//Register system event listeners
	this->services.sysEventQueue.RegisterListener(this->sceneManager.GetListener());

	//Init renderer
	this->renderer.Init(this->services.appSettings.windowWidth, this->services.appSettings.windowHeight, "Hardic", &this->services);

	//Load resources
	this->services.resources.LoadResourcesFromFolder("data");

	//Create scenes
	this->sceneManager.SetServices(&this->services);
	//Create levels
	std::vector<char> lvlListData = this->services.resources.GetResNode("misc/level_list.dat")->data.data;
	std::string lvlListStr(lvlListData.begin(), lvlListData.end());
	std::vector<std::string> lvlList = Utilities::Split(lvlListStr, '\n');
	for (int i = 0; i < lvlList.size(); i++) {
		LevelScene* level = new LevelScene();
		level->name = lvlList[i].substr(0, lvlList[i].find_last_of('\r'));
		this->sceneManager.AddScene(level);
	}
	this->sceneManager.SetActiveScene(lvlList[0]);
	//Render loop
	this->renderer.SetClearColor(glm::vec3(0.2f, 0.2f, 0.2f));
	while (!this->renderer.GetWindowClose()) {
		if (this->services.input.GetKey(GLFW_KEY_LEFT_CONTROL)) {
			//Reload resources if ctrl-r
			if (this->services.input.GetKeyDown(GLFW_KEY_R)) {
				this->services.resources = Resources();
				this->services.resources.LoadResourcesFromFolder("data");
			}
		}
		//Clear buffers
		this->renderer.ClearBuffers();
		//Poll input
		this->renderer.PollInput();
		//Update scene
		this->sceneManager.GetActiveScene()->UpdateScene();
		//Add render objects
		this->renderer.SetRenderObjects(this->sceneManager.GetActiveScene()->GetGraphicsCache());
		//Render
		this->renderer.Render();
	}
}


Game::~Game()
{
}
