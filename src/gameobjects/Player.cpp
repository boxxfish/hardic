#include "stdafx.h"
#include "Player.h"


Player::Player() : GameObject()
{
	this->GetTransform()->name = "Player";
}


Player::~Player()
{
}

void Player::Start() {
	this->SetGraphic(&this->graphic);
	//Set sprites
	Texture tex = Texture::FindTexture("PlayerSprites");
	SpriteSheet playerSprites = SpriteSheet(tex, 64, 128);
	this->front = playerSprites.GetSprite(1);
	this->back = playerSprites.GetSprite(0);
	this->left = playerSprites.GetSprite(2);
	this->right = playerSprites.GetSprite(3);
	this->graphic.SetSprite(this->back);
	//Set transform
	this->GetTransform()->data.SetScale(glm::vec3(0.01, 0.01, 0.01));
	this->GetTransform()->data.SetRotation(glm::vec3(glm::radians(135.0f), 0, 0));
	this->GetSceneEventQueue()->Broadcast(new CameraTargetEvent(&this->GetTransform()->data, true));
}

void Player::Update()
{
	//Movement
	if (this->GetServices()->input.GetKeyDown(GLFW_KEY_LEFT)) {
		this->position.x--;
		this->graphic.SetSprite(this->left);
	}
	if (this->GetServices()->input.GetKeyDown(GLFW_KEY_RIGHT)) {
		this->position.x++;
		this->graphic.SetSprite(this->right);
	}
	if (this->GetServices()->input.GetKeyDown(GLFW_KEY_UP)) {
		this->position.y++;
		this->graphic.SetSprite(this->back);
	}
	if (this->GetServices()->input.GetKeyDown(GLFW_KEY_DOWN)) {
		this->position.y--;
		this->graphic.SetSprite(this->front);
	}
	this->GetTransform()->data.SetPosition(glm::vec3(this->position.x, 0.2f, -this->position.y));
}
