#pragma once

#include <engine/GameObject.h>
#include <graphics/SpriteGraphic.h>
#include <eventsystem/CameraTargetEvent.h>

class Player : public GameObject
{
public:
	Player();
	~Player();

	void Start();
	void Update();
private:
	SpriteGraphic graphic;
	SpriteSheet::Sprite front, back, left, right;
	glm::ivec2 position;
};