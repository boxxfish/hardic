#pragma once

#include <engine/GameObject.h>
#include <objconverter/ObjConverter.h>

class ObjConverterObject : public GameObject
{
public:
	ObjConverterObject();
	~ObjConverterObject();

	void Start();
	void Update();
};

