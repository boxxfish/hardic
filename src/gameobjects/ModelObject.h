#pragma once

#include <list>

#include <engine/GameObject.h>
#include <Model.h>
#include <gameobjects/MeshObject.h>
#include <eventsystem/GameObjectCreationEvent.h>

class ModelObject : public GameObject
{
public:
	ModelObject();
	ModelObject(Model model);
	~ModelObject();

	void Start();
private:
	Model model;
	std::list<MeshObject> meshObjs;
};

