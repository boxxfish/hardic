#pragma once

#include <list>

#include <engine/GameObject.h>
#include <LevelData.h>
#include <gameobjects/ModelObject.h>

class Level : public GameObject
{
public:
	Level();
	Level(LevelData* levelData);
	~Level();

	void Start();
private:
	LevelData* levelData;
	std::list<ModelObject> tiles;
};