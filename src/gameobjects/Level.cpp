#include "stdafx.h"
#include "Level.h"


Level::Level() : GameObject()
{
}

Level::Level(LevelData* levelData) : GameObject() {
	this->levelData = levelData;
	this->GetTransform()->name = "Level";
}


Level::~Level()
{
}

void Level::Start()
{
	float tileSize = 1.0f;
	//Iterate over tile layer
	for (int i = 0; i < this->levelData->map.numTileLayers; i++) {
		for (int y = 0; y < this->levelData->map.height; y++) {
			for (int x = 0; x < this->levelData->map.width; x++) {
				int currTile = this->levelData->map.tileLayer[i][y][x];
				//Skip if 0
				if (currTile == 0)
					continue;
				//Add tile
				ModelObject tileObj(this->levelData->map.tileset.tiles[currTile]);
				tileObj.GetTransform()->data.Translate(glm::vec3(x * tileSize, 0, y * tileSize));
				tileObj.GetTransform()->name = "Tile " + std::to_string(x) + ", " + std::to_string(y);
				this->tiles.push_back(tileObj);
				this->GetServices()->sysEventQueue.Broadcast(new GameObjectCreationEvent(&this->tiles.back(), this));
			}
		}
	}
}
