#pragma once

#include <engine/GameObject.h>
#include <graphics/MeshGraphic.h>

class MeshObject : public GameObject
{
public:
	MeshObject();
	MeshObject(std::vector<float> coords, Texture tex);
	~MeshObject();

	void Start();
private:
	MeshGraphic graphic;
};

