#include <stdafx.h>
#include "MeshObject.h"

MeshObject::MeshObject() {
}

MeshObject::MeshObject(std::vector<float> coords, Texture tex) {
	this->graphic.SetCoords(coords);
	this->graphic.SetTexture(tex);
}

MeshObject::~MeshObject()
{
}

void MeshObject::Start() {
	this->SetGraphic(&this->graphic);
}
