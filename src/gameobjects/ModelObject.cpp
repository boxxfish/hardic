#include <stdafx.h>
#include "ModelObject.h"

ModelObject::ModelObject() : GameObject() {
}

ModelObject::ModelObject(Model model) : GameObject() {
	this->model = model;
}

ModelObject::~ModelObject()
{
}

void ModelObject::Start()
{
	for (int i = 0; i < this->model.meshes.size(); i++) {
		this->meshObjs.push_back(MeshObject(this->model.meshes[i].GetCoords(), Texture::FindTexture(this->model.meshes[i].tex)));
		this->GetServices()->sysEventQueue.Broadcast(new GameObjectCreationEvent(&this->meshObjs.back(), this));
	}
}
