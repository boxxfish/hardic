#include "stdafx.h"
#include "ObjConverterObject.h"


ObjConverterObject::ObjConverterObject() : GameObject()
{
	this->GetTransform()->name = "ObjectConverter";
}


ObjConverterObject::~ObjConverterObject()
{
}

void ObjConverterObject::Start() {
}

void ObjConverterObject::Update() {
	if (this->GetServices()->input.IsFileDropped()) {
		for (int i = 0; i < this->GetServices()->input.GetFileCount(); i++) {
			//Check extension
			std::string path = this->GetServices()->input.GetFilePath(i);
			std::string ext = path.substr(path.find_last_of('.') + 1);
			if (ext == "obj" || ext == "dae") {
				ObjConverter converter(path);
				converter.WriteFile();
			}
		}
	}
}
