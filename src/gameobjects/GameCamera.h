#pragma once

#include <engine/GameObject.h>
#include <engine/PerspCamera.h>
#include <eventsystem/CameraTargetEvent.h>

class GameCamera : public GameObject
{
public:
	GameCamera();
	~GameCamera();

	void Start();
	void Update();
	void LateUpdate();
	void HandleEvent(Event* evt);
private:
	Transform* target;
	float offset;
	float lerpAmount;
	bool targetSet;
	float pitch;
	glm::vec3 focusPos;
};