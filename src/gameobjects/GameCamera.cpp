#include "stdafx.h"
#include "GameCamera.h"

GameCamera::GameCamera()
{
	this->GetTransform()->name = "GameCamera";
	this->ReceiveEvents();
	this->targetSet = false;
}

GameCamera::~GameCamera()
{
}

void GameCamera::Start()
{
	//Create scene camera
	this->GetServices()->camera = new PerspCamera(this->GetServices()->appSettings.windowWidth, this->GetServices()->appSettings.windowHeight, 45);
	//Camera values
	this->offset = -5;
	this->lerpAmount = 0.5f;
	this->pitch = glm::radians(45.0f);
	//Set position and rotation
	if(this->targetSet)
		this->focusPos = this->target->GetPosition();
	this->GetTransform()->data.SetRotation(glm::vec3(1, 0, 0) * this->pitch);
}

void GameCamera::Update()
{
	//Debug camera
	/*float speed = 0.1f * this->GetServices()->time.GetDeltaTime();
	if (this->GetServices()->input.GetKey(GLFW_KEY_LEFT_SHIFT) || this->GetServices()->input.GetKey(GLFW_KEY_RIGHT_SHIFT))
		speed *= 4;
	if (this->GetServices()->input.GetKey(GLFW_KEY_LEFT))
		this->GetTransform()->data.Translate(glm::vec3(1, 0, 0) * speed);
	if (this->GetServices()->input.GetKey(GLFW_KEY_RIGHT))
		this->GetTransform()->data.Translate(glm::vec3(-1, 0, 0) * speed);
	if (this->GetServices()->input.GetKey(GLFW_KEY_UP))
		this->GetTransform()->data.Translate(glm::vec3(0, -1, 0) * speed);
	if (this->GetServices()->input.GetKey(GLFW_KEY_DOWN))
		this->GetTransform()->data.Translate(glm::vec3(0, 1, 0) * speed);
	if (this->GetServices()->input.GetKey(GLFW_KEY_S))
		this->GetTransform()->data.Translate(glm::vec3(0, 0, -1) * speed);
	if (this->GetServices()->input.GetKey(GLFW_KEY_W))
		this->GetTransform()->data.Translate(glm::vec3(0, 0, 1) * speed);*/
	//Change rotation
	if (this->GetServices()->input.GetKey(GLFW_KEY_W)) {
		this->pitch += glm::radians(5.0f);
		this->GetTransform()->data.SetRotation(glm::vec3(1, 0, 0) * this->pitch);
	}
	if (this->GetServices()->input.GetKey(GLFW_KEY_S)) {
		this->pitch -= glm::radians(5.0f);
		this->GetTransform()->data.SetRotation(glm::vec3(1, 0, 0) * this->pitch);
	}
	//Set camera properties
	this->GetServices()->camera->view = this->GetTransform()->data;
	//Set global uniforms
	Material::SetGlobalUniform("projMat", this->GetServices()->camera->GetProjection());
	//Recalculate camera matrix
	glm::mat4 matrix;
	matrix = glm::scale(matrix, this->GetTransform()->data.GetScale());
	glm::vec3 rotation = this->GetTransform()->data.GetRotation();
	matrix = glm::rotate(matrix, rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
	matrix = glm::rotate(matrix, rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
	matrix = glm::rotate(matrix, rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));
	matrix = glm::translate(matrix, this->GetTransform()->data.GetPosition());
	Material::SetGlobalUniform("viewMat", matrix);
}

void GameCamera::LateUpdate() {
	//Set position to target
	this->focusPos = (this->focusPos - this->target->GetPosition()) * this->lerpAmount;
	//Move camera back on axis
	glm::vec3 moveBack = glm::vec3(glm::rotate(glm::mat4(), this->pitch, glm::vec3(1, 0, 0)) * glm::vec4(0, -4, 0, 1));
	this->GetTransform()->data.SetPosition(this->focusPos + moveBack);
}

void GameCamera::HandleEvent(Event* evt) {
	if (CameraTargetEvent* camEvt = dynamic_cast<CameraTargetEvent*>(evt)) {
		this->target = camEvt->target;
		this->targetSet = true;
		if (camEvt->instant) {
			this->focusPos = this->target->GetPosition();
		}
	}
}
