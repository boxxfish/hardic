#pragma once

#include "GameObject.h"
#include "TextGraphic.h"

class TextObject : public GameObject
{
public:
	TextObject();
	~TextObject();

	void SetText(std::string text);

	void SetFont(Font font);

	void SetLineSpacing(float spacing);

	void SetLineWidth(float width);

	void SetRenderOrder(int order);

	void SetAlign(TextGraphic::AlignType type);

	void SetTint(glm::vec4 tint);

	void Start();
private:
	TextGraphic textGraphic;
};

