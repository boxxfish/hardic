#include "stdafx.h"
#include "TextObject.h"


TextObject::TextObject() : GameObject() {
	this->GetTransform()->name = "Text";
}

TextObject::~TextObject() {
}

void TextObject::SetText(std::string text) {
	this->GetTransform()->name = text;
	this->textGraphic.DisplayText(text);
}

void TextObject::SetFont(Font font) {
	this->textGraphic.SetFont(font);
}

void TextObject::SetLineSpacing(float spacing) {
	this->textGraphic.SetLineSpacing(spacing);
}

void TextObject::SetLineWidth(float width) {
	this->textGraphic.SetLineWidth(width);
}

void TextObject::SetRenderOrder(int order) {
	this->textGraphic.SetRenderOrder(order);
}

void TextObject::SetAlign(TextGraphic::AlignType type) {
	this->textGraphic.SetTextAlign(type);
}

void TextObject::SetTint(glm::vec4 tint) {
	this->textGraphic.tint = tint;
}

void TextObject::Start() {
	this->SetGraphic(&this->textGraphic);
}
