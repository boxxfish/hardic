#pragma once

#include "engine/Scene.h"
#include "engine/Renderer.h"
#include "scenes/LevelScene.h"
#include "engine/Resources.h"
#include "engine/SceneManager.h"
#include "engine/Services.h"
#include <scenes/EditorScene.h>

class Game
{
public:
	Game();
	~Game();
private:
	Renderer renderer;
	Services services;
	SceneManager sceneManager;
};