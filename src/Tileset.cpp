#include "stdafx.h"
#include "Tileset.h"


Tileset::Tileset()
{
}

Tileset::Tileset(std::vector<char> data, Resources* resources) {
	Importer importer(data);

	//Header
	if ((!(importer.ReadChar() == 'T' && importer.ReadChar() == 'L' && importer.ReadChar() == 'S' && importer.ReadChar() == 'T'))) {
		std::cout << "Error: File is not a valid level tileset file.\n";
		return;
	}

	//Number of tiles
	int numTiles = importer.ReadUint8();
	//Null tile at 0
	this->tiles.push_back(Model());
	for (int i = 0; i < numTiles; i++) {
		//Model name
		std::string modelName = importer.ReadString();
		Model model = SmshImporter::Import(resources->GetResNode("models/" + modelName + ".dat")->data.data);
		this->tiles.push_back(model);
	}
}


Tileset::~Tileset()
{
}
