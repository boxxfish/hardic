#pragma once

#include <string>

#include <engine/Scene.h>
#include <eventsystem/GameObjectCreationEvent.h>

#include <gameobjects/player.h>
#include <gameobjects/Level.h>
#include <gameobjects/GameCamera.h>

class LevelScene : public Scene
{
public:
	LevelScene();
	~LevelScene();

	void Start();
private:
	Player player;
	Level level;
	LevelData levelData;
	GameCamera cam;
};

