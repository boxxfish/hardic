#include "stdafx.h"
#include "LevelScene.h"

LevelScene::LevelScene() : Scene()
{
}


LevelScene::~LevelScene()
{
}

void LevelScene::Start() {
	//Init level data
	LevelMap map = LevelMap(this->GetServices()->resources.GetResNode("maps/" + this->name + ".dat")->data.data, &this->GetServices()->resources);
	this->levelData = LevelData(map);
	
	//Add gameobjects to scene
	this->level = Level(&this->levelData);
	this->AddGameObject(&this->level);
	this->AddGameObject(&this->player);
	this->AddGameObject(&this->cam);
}
