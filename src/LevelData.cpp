#include "stdafx.h"
#include "LevelData.h"


LevelData::LevelData()
{
}

LevelData::LevelData(LevelMap map) {
	//Set map
	this->map = map;

	//Init layers
	this->tileLayer = ObjectLayer<unsigned int>(map.width, map.height, 0);
	this->objectLayer = ObjectLayer<unsigned int>(map.width, map.height, 0);

	//Fill data
	for (int i = 0; i < map.numTileLayers;i++) {
		for (int y = 0; y < map.height; y++) {
			for (int x = 0; x < map.width; x++) {
				this->tileLayer.SetValue(x, y, map.tileLayer[i][y][x]);
				this->objectLayer.SetValue(x, y, map.objectLayer[y][x]);
			}
		}
	}
}


LevelData::~LevelData()
{
}
