#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Transform
{
public:
	Transform();
	~Transform();

	///<summary>
	///Returns local matrix.
	///</summary>
	glm::mat4 GetLocalMatrix();

	///<summary>
	///Returns global matrix.
	///</summary>
	glm::mat4 GetGlobalMatrix();

	///<summary>
	///Sets global matrix.
	///</summary>
	void SetGlobalMatrix(glm::mat4 globalMat);

	void Translate(glm::vec3 direction);

	void Scale(glm::vec3 scale);

	void SetPosition(glm::vec3 position);
	void SetRotation(glm::vec3 rotation);
	void SetScale(glm::vec3 scale);

	glm::vec3 GetPosition();
	glm::vec3 GetRotation();
	glm::vec3 GetScale();
private:
	glm::vec3 position;
	glm::vec3 rotation;
	glm::vec3 scale;
	glm::mat4 localMat;
	glm::mat4 globalMat;
	bool dirty;
};

