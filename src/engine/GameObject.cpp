#include "stdafx.h"
#include "GameObject.h"

GameObject::GameObject() {
	this->useGraphic = false;
	this->receivingEvents = false;
	this->transform = std::make_shared<TreeNode<Transform>>();
}


GameObject::~GameObject()
{
}

std::shared_ptr<TreeNode<Transform>> GameObject::GetTransform() {
	return this->transform;
}

void GameObject::SetServices(Services* services) {
	this->services = services;
}

Graphic* GameObject::GetGraphic() {
	return this->graphic;
}

bool GameObject::UseGraphic() {
	return this->useGraphic;
}

bool GameObject::IsReceivingEvents() {
	return this->receivingEvents;
}

void GameObject::SetSceneEventQueue(EventQueue* eventQueue) {
	this->sceneEventQueue = eventQueue;
}

void GameObject::SetGraphic(Graphic* graphic) {
	this->graphic = graphic;
	this->graphic->transform = this->transform;
	this->useGraphic = true;
	this->graphic->SetServices(this->services);
}

EventQueue* GameObject::GetSceneEventQueue() {
	return this->sceneEventQueue;
}

void GameObject::ReceiveEvents() {
	this->receivingEvents = true;
}

Services* GameObject::GetServices() {
	return this->services;
}

void GameObject::Start() {
}

void GameObject::Update() {
}

void GameObject::LateUpdate() {
}

void GameObject::HandleEvent(Event * evt) {
}
