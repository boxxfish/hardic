#pragma once

#include <memory>
#include <iostream>

#include "Transform.h"
#include "TreeNode.h"
#include "Graphic.h"
#include "Services.h"
#include <eventsystem/EventQueue.h>

class GameObject
{
public:
	GameObject();
	~GameObject();

	///<summary>
	///Returns a pointer to the transform.
	///</summary>
	std::shared_ptr<TreeNode<Transform>> GetTransform();

	///<summary>
	///Sets services.
	///</summary>
	void SetServices(Services* services);

	///<summary>
	///Returns the gameobject's graphic.
	///</summary>
	Graphic* GetGraphic();

	///<summary>
	///Does the gameobject use a graphic?
	///</summary>
	bool UseGraphic();

	///<summary>
	///Is the gameobject receiving events?
	///</summary>
	bool IsReceivingEvents();

	///<summary>
	///Sets a reference to the scene's event queue.
	///</summary>
	void SetSceneEventQueue(EventQueue* eventQueue);

	///<summary>
	///Function gets called once scene is initialized.
	///</summary>
	virtual void Start();

	///<summary>
	///Function gets called once per loop before transform propagation.
	///</summary>
	virtual void Update();

	///<summary>
	///Function gets called once per loop after transform propagation.
	///</summary>
	virtual void LateUpdate();

	///<summary>
	///Function gets called if the gameobject is allowed to receive events.
	///</summary>
	virtual void HandleEvent(Event* evt);
protected:
	///<summary>
	///Sets the gameobject's graphic.
	///</summary>
	void SetGraphic(Graphic* graphic);

	///<summary>
	///Returns the scene's event queue.
	///</summary>
	EventQueue* GetSceneEventQueue();

	///<summary>
	///Allows gameobject to receive events.
	///</summary>
	void ReceiveEvents();

	///<summary>
	///Returns services.
	///</summary>
	Services* GetServices();
private:
	Graphic* graphic;
	bool useGraphic;
	std::shared_ptr<TreeNode<Transform>> transform;
	Services* services;
	EventQueue* sceneEventQueue;
	bool receivingEvents;
};

