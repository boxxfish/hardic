#include "stdafx.h"
#include "Renderer.h"

Renderer::Renderer() {
	
}


Renderer::~Renderer() {
	glfwTerminate();
}

void Renderer::Init(int width, int height, std::string title, Services* services) {
	//Set services
	this->services = services;

	//Initialize GLFW
	if (!glfwInit()) {
		std::cout << "Error: Could not initialize GLFW.\n";
		glfwTerminate();
		return;
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_RESIZABLE, false);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_SAMPLES, 4);

	//Set up window
	this->window = glfwCreateWindow(width, height, title.c_str(), NULL, NULL);
	if (this->window == nullptr) {
		std::cout << "Error: Failed to create window.\n";
		return;
	}
	glfwMakeContextCurrent(this->window);
	glfwGetWindowSize(this->window, &this->width, &this->height);

	//Initialize GLEW
	glewExperimental = true;
	if (glewInit() != GLEW_OK) {
		std::cout << "Error: Could not initialize GLEW.\n";
		return;
	}

	//Set callbacks
	glfwSetKeyCallback(this->window, (GLFWkeyfun)Input::KeyboardHandler);
	glfwSetMouseButtonCallback(this->window, (GLFWmousebuttonfun)Input::MouseButtonHandler);
	glfwSetCursorPosCallback(this->window, (GLFWcursorposfun)Input::CursorPositionHandler);
	glfwSetDropCallback(this->window, (GLFWdropfun)Input::FileDropHandler);

	//Finish setup
	glViewport(0, 0, this->width, this->height);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_MULTISAMPLE);
	this->clearColor = glm::vec3(0.2f, 0.2f, 0.2f);
	this->services->time.SetTime(0);
}

void Renderer::Render(bool swapBuffers) {
	//Set current time
	this->services->time.SetTime(glfwGetTime());
	//Order renderobjects from high to low
	/*std::vector<RenderObject> temp;
	int count = this->renderObjects.size();
	for (int i = 0; i < count; i++) {
		int highIndex = 0;
		for (int j = 0; j < this->renderObjects.size(); j++) {
			if (this->renderObjects[highIndex].order < this->renderObjects[j].order)
				highIndex = j;
		}
		temp.push_back(this->renderObjects[highIndex]);
		this->renderObjects.erase(this->renderObjects.begin() + highIndex);
	}
	//Replace renderobject vector with temp vector
	this->renderObjects = temp;*/
	//Render renderobjects from last to first
	for (int i = this->renderObjects.size() - 1; i >= 0; i--) {
		glUseProgram(this->renderObjects[i].material.GetProgram());
		this->renderObjects[i].material.SetUniforms();
		glBindVertexArray(this->renderObjects[i].mesh.GetVAO());
		glDrawArrays(GL_TRIANGLES, 0, this->renderObjects[i].mesh.GetCoordCount());
		glBindVertexArray(0);
	}
	this->renderObjects.clear();

	//Swap buffers
	if(swapBuffers)
		glfwSwapBuffers(this->window);
}

bool Renderer::GetWindowClose() {
	return glfwWindowShouldClose(this->window);
}

void Renderer::SetClearColor(glm::vec3 clearColor) {
	this->clearColor = clearColor;
}

void Renderer::AddRenderObject(RenderObject obj) {
	this->renderObjects.push_back(obj);
}

void Renderer::AddRenderObjects(std::vector<RenderObject> vector) {
	this->renderObjects.insert(this->renderObjects.end(), vector.begin(), vector.end());
}

void Renderer::SetRenderObjects(std::vector<RenderObject> vector) {
	this->renderObjects = vector;
}

void Renderer::PollInput() {
	this->services->input.Update();
	glfwPollEvents();
}

void Renderer::ClearBuffers() {
	//Set clear color
	glClearColor(this->clearColor.r, this->clearColor.g, this->clearColor.b, 1.0f);

	//Clear buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

int Renderer::GetWidth() {
	return this->width;
}

int Renderer::GetHeight() {
	return this->height;
}

float Renderer::GetAspectRatio() {
	return (float)this->width / this->height;
}
