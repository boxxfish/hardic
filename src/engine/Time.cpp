#include "stdafx.h"
#include "Time.h"

float Time::GetDeltaTime() {
	float deltaTime = this->currTime - this->lastTime;
	//Clamp delta time at 5 fps
	return deltaTime < 0.2f ? deltaTime : 0.2f;
}

float Time::GetTime() {
	return this->currTime;
}

void Time::SetTime(float time) {
	this->lastTime = this->currTime;
	this->currTime = time;
}
