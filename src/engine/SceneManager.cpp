#include "stdafx.h"
#include "SceneManager.h"


SceneManager::SceneManager()
{
}


SceneManager::~SceneManager()
{
}

void SceneManager::AddScene(Scene* scene) {
	scene->SetServices(this->services);
	this->scenes[scene->name] = scene;
}

void SceneManager::SetActiveScene(std::string name) {
	this->activeScene = this->scenes[name];
	this->eventListener.activeScene = this->activeScene;
	this->scenes[name]->Init();
}

Scene* SceneManager::GetActiveScene() {
	return this->activeScene;
}

void SceneManager::SetServices(Services* services) {
	this->services = services;
}

Services* SceneManager::GetServices() {
	return this->services;
}

SceneManagerEventListener* SceneManager::GetListener() {
	return &this->eventListener;
}
