#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>

#include "Transform.h"
#include <engine/Camera.h>

class OrthoCamera : public Camera
{
public:
	OrthoCamera();
	OrthoCamera(int width, int height, float size);
	~OrthoCamera();

	///<summary>
	///Returns projection matrix.
	///</summary>
	glm::mat4 GetProjection();

	///<summary>
	///Sets camera size.
	///</summary>
	void SetSize(float size);

	///<summary>
	///Returns camera size.
	///</summary>
	float GetSize();

	///<summary>
	///Returns window width.
	///</summary>
	int GetWindowWidth();

	///<summary>
	///Returns window height.
	///</summary>
	int GetWindowHeight();

	glm::vec2 ScreenToWorld(glm::vec2 screenPoint);

	glm::vec2 ScreenProject(glm::vec2 screenPoint);
private:
	float aspect;
	int width, height;
	float size; //size is half the camera's vertical size
};

