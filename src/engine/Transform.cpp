#include "stdafx.h"
#include "Transform.h"


Transform::Transform() {
	this->scale = glm::vec3(1.0f, 1.0f, 1.0f);
	this->dirty = true;
}


Transform::~Transform()
{
}

glm::mat4 Transform::GetLocalMatrix() {
	if (this->dirty) {
		this->dirty = false;
		glm::mat4 matrix;
		matrix = glm::translate(matrix, this->position);
		matrix = glm::rotate(matrix, this->rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
		matrix = glm::rotate(matrix, this->rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
		matrix = glm::rotate(matrix, this->rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));
		matrix = glm::scale(matrix, this->scale);
		this->localMat = matrix;
	}
	return this->localMat;
}

glm::mat4 Transform::GetGlobalMatrix() {
	return this->globalMat * this->GetLocalMatrix();
}

void Transform::SetGlobalMatrix(glm::mat4 globalMat) {
	this->globalMat = globalMat;
}

void Transform::Translate(glm::vec3 direction) {
	this->dirty = true;
	this->position += direction;
}

void Transform::Scale(glm::vec3 scale) {
	this->dirty = true;
	this->scale.x *= scale.x;
	this->scale.y *= scale.y;
	this->scale.z *= scale.z;
}

void Transform::SetPosition(glm::vec3 position) {
	this->dirty = true;
	this->position = position;
}

void Transform::SetRotation(glm::vec3 rotation) {
	this->dirty = true;
	this->rotation = rotation;
}

void Transform::SetScale(glm::vec3 scale) {
	this->dirty = true;
	this->scale = scale;
}

glm::vec3 Transform::GetPosition() {
	return this->position;
}

glm::vec3 Transform::GetRotation() {
	return this->rotation;
}

glm::vec3 Transform::GetScale() {
	return this->scale;
}
