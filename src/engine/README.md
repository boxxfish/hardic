# How the heck do I use this engine? #

## Scenes ##

A scene is a collection of gameobjects. To make a unique scene (like a menu screen or level), you just derive from it. Doing that'll give you a couple functions to use:
    
    * void AddGameObject(GameObject* object, bool useParent = false, std::string parentName = "")
        * Adds a gameobject to the scene. If you set useParent to true, you can set the gameobject's parent useing their name.
    * GameObject* FindGameObject(std::string name)
        * Returns the first gameobject in the scene with this name.
    * void RemoveGameObject(GameObject* g)
        * Removes the gameobject from the scene.
    * void Start()
        * Put whatever initialization code you need in here that requires the engine to be running. This gets called before any of the gameobjects in the scene get started.
    * void Update()
        * Put whatever updating code you need in here. This gets called before any of the gameobjects in the scene get updated.

Once you're done with your derived scene class, add it to the game by calling Scene::AddScene(&YourScene) in the main game class. Activate it by using Scene::SetActiveScene(std::string sceneName).

Here's an example:

LevelScene::LevelScene() : Scene() {
	this->name = "Prologue_Classroom";
}

LevelScene::~LevelScene()
{
}

void LevelScene::Start() {
	//Add gameobjects
	this->level = Level(Resources::GetTiledMap("prologue_classroom.tmx"));
	this->AddGameObject(&this->level);
	this->AddGameObject(&this->selector);
	this->AddGameObject(&this->player);
	this->gameCamera = GameCamera(720, 405);
	this->AddGameObject(&this->gameCamera);
	this->AddGameObject(&this->hud);

	//Set gamecamera target
	this->gameCamera.SetTarget(&this->player.GetTransform()->data, true);
}

void LevelScene::Update() {

}

## GameObjects ##

A gameobject is an entity in the scene. To make a unique gameobject (like a button or player), you just derive from it. Doing that'll give you a couple functions to use:
    
    * void Start()
        * Put whatever initialization code you need in here that requires the engine to be running.
    * void Update()
        * Put whatever updating code you need in here.
    * void LateUpdate()
        * Put whatever updating code you need in here after all gameobjects have Update called and the transforms have propagated.
    * std::shared_ptr<TreeNode<Transform>> GetTransform()
        * Returns the gameobject's transform.
    * void SetGraphic(Graphic* graphic)
        * Sets the gameobject's graphic.