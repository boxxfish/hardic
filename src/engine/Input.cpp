#include "stdafx.h"
#include "Input.h"

bool Input::keys[512];
bool Input::lastKeys[512];
bool Input::mButtons[3];
bool Input::lastMButtons[3];
float Input::cursorXpos;
float Input::cursorYpos;
std::string Input::filePaths[10];
unsigned short int Input::fileCount;

bool Input::GetKey(int key) {
	return Input::keys[key];
}

bool Input::GetKeyDown(int key) {
	return !Input::lastKeys[key] && Input::keys[key];
}

bool Input::GetKeyUp(int key) {
	return Input::lastKeys[key] && !Input::keys[key];
}

bool Input::GetMouseButton(int button) {
	return Input::mButtons[button];
}

bool Input::GetMouseButtonDown(int button) {
	return !Input::lastMButtons[button] && Input::mButtons[button];
}

bool Input::GetMouseButtonUp(int button) {
	return Input::lastMButtons[button] && !Input::mButtons[button];
}

float Input::GetCursorXPos() {
	return Input::cursorXpos;
}

float Input::GetCursorYPos() {
	return Input::cursorYpos;
}

bool Input::IsFileDropped() {
	return Input::fileCount != 0;
}

unsigned short int Input::GetFileCount() {
	return Input::fileCount;
}

std::string Input::GetFilePath(int index) {
	assert(index < Input::fileCount);
	return Input::filePaths[index];
}

void Input::Update() {
	Input::fileCount = 0;
	std::memcpy(Input::lastKeys, Input::keys, 512);
	std::memcpy(Input::lastMButtons, Input::mButtons, 3);
}

void Input::KeyboardHandler(GLFWwindow* window, int key, int scancode, int action, int mods) {
	assert(!(key < 0 || key >= 512));
	if(action == GLFW_PRESS)
		Input::keys[key] = true;
	if(action == GLFW_RELEASE)
		Input::keys[key] = false;
}

void Input::MouseButtonHandler(GLFWwindow* window, int button, int action, int mods) {
	assert(!(button < 0 || button >= 3));
	if (action == GLFW_PRESS)
		Input::mButtons[button] = true;
	if (action == GLFW_RELEASE)
		Input::mButtons[button] = false;
}

void Input::CursorPositionHandler(GLFWwindow* window, double xpos, double ypos) {
	Input::cursorXpos = xpos;
	Input::cursorYpos = ypos;
}

void Input::FileDropHandler(GLFWwindow* window, int fileCount, char** paths) {
	for (int i = 0; i < fileCount; i++)
		Input::filePaths[i] = paths[i];
	Input::fileCount = fileCount;
}
