#pragma once

#include "Renderer.h"
#include "Transform.h"
#include "TreeNode.h"

class Graphic
{
public:
	Graphic();
	~Graphic();

	///<summary>
	///Updates graphic.
	///</summary>
	virtual void Update();

	///<summary>
	///Sets render order.
	///</summary>
	virtual void SetRenderOrder(unsigned int order);

	///<summary>
	///Sets services.
	///</summary>
	void SetServices(Services* services);

	RenderObject renderObject;
	std::shared_ptr<TreeNode<Transform>> transform;
	Services* services;
};