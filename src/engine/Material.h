#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <memory>

#include "Shader.h"
#include "Texture.h"

class Material
{
public:
	Material();
	Material(Shader shader);
	~Material();

	///<summary>
	///Returns program.
	///</summary>
	GLuint GetProgram();

	///<summary>
	///Sets uniforms.
	///</summary>
	void SetUniforms();

	//Uniform setting functions.
	void SetUniform(std::string name, float value);
	void SetUniform(std::string name, glm::vec2 value);
	void SetUniform(std::string name, glm::vec3 value);
	void SetUniform(std::string name, glm::vec4 value);
	void SetUniform(std::string name, glm::mat4 value);
	//Global uniform setting functions.
	static void SetGlobalUniform(std::string name, float value);
	static void SetGlobalUniform(std::string name, glm::vec2 value);
	static void SetGlobalUniform(std::string name, glm::vec3 value);
	static void SetGlobalUniform(std::string name, glm::vec4 value);
	static void SetGlobalUniform(std::string name, glm::mat4 value);
	//Sets texture.
	void SetTexture(std::string name, Texture texture, int texUnit);
private:
	std::map <std::string, float> floatUniforms;
	std::map <std::string, glm::vec2> vec2Uniforms;
	std::map <std::string, glm::vec3> vec3Uniforms;
	std::map <std::string, glm::vec4> vec4Uniforms;
	std::map <std::string, glm::mat4> mat4Uniforms;
	std::map <std::string, int> intUniforms;
	static std::map <std::string, float> floatGlobalUniforms;
	static std::map <std::string, glm::vec2> vec2GlobalUniforms;
	static std::map <std::string, glm::vec3> vec3GlobalUniforms;
	static std::map <std::string, glm::vec4> vec4GlobalUniforms;
	static std::map <std::string, glm::mat4> mat4GlobalUniforms;
	Shader shader;
	Texture textures[8];
	bool units[8];
};

