#include "stdafx.h"
#include "Shader.h"

std::map<std::string, Shader> Shader::cache;

Shader::Shader() {
}

Shader::Shader(std::string vSrc, std::string fSrc) {
	//Vertex Shader
	GLuint vShad = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* vSrcStr = vSrc.c_str();
	glShaderSource(vShad, 1, &vSrcStr, NULL);
	glCompileShader(vShad);
	GLint vOK;
	glGetShaderiv(vShad, GL_COMPILE_STATUS, &vOK);
	if (!vOK) {
		GLchar msg[512];
		glGetShaderInfoLog(vShad, 512, NULL, msg);
		std::cout << "Error: Could not compile vertex shader.\n" << msg << "\n";
	}

	//Fragment Shader
	GLuint fShad = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar* fSrcStr = fSrc.c_str();
	glShaderSource(fShad, 1, &fSrcStr, NULL);
	glCompileShader(fShad);
	GLint fOK;
	glGetShaderiv(fShad, GL_COMPILE_STATUS, &fOK);
	if (!fOK) {
		GLchar msg[512];
		glGetShaderInfoLog(fShad, 512, NULL, msg);
		std::cout << "Error: Could not compile fragment shader.\n" << msg << "\n";
	}

	//Link program
	this->program = glCreateProgram();
	glAttachShader(this->program, vShad);
	glAttachShader(this->program, fShad);
	glLinkProgram(this->program);
	GLint pOK;
	glGetProgramiv(this->program, GL_LINK_STATUS, &pOK);
	if (!pOK) {
		GLchar msg[512];
		glGetProgramInfoLog(this->program, 512, NULL, msg);
		std::cout << "Error: Could not link program.\n" << msg << "\n";
	}
	glDeleteShader(vShad);
	glDeleteShader(fShad);
}


Shader::~Shader()
{
}

GLuint Shader::GetProgram() {
	return this->program;
}

void Shader::Destroy() {
	glDeleteProgram(this->program);
}

void Shader::CreateShader(std::string name, std::string vSrc, std::string fSrc) {
	if (Shader::cache.find(name) != Shader::cache.end())
		Shader::cache[name].Destroy();
	Shader::cache[name] = Shader(vSrc, fSrc);
}

Shader Shader::FindShader(std::string name) {
	if (Shader::cache.find(name) == Shader::cache.end()) {
		std::cout << "Error: Could not find shader named " << name << ".\n";
		return Shader();
	}
	return Shader::cache[name];
}

void Shader::DeleteShader(std::string name) {
	if (Shader::cache.find(name) == Shader::cache.end())
		return;
	Shader::cache[name].Destroy();
}

void Shader::ClearCache() {
	std::map<std::string, Shader>::iterator it;
	for (it = Shader::cache.begin(); it != Shader::cache.end(); it++)
		it->second.Destroy();
}
