#include "stdafx.h"
#include "Texture.h"

std::map<std::string, Texture> Texture::cache;

Texture::Texture()
	: width(0),
	height(0),
	texture(0)
{}

Texture::Texture(unsigned int width, unsigned int height, std::vector<unsigned char> pixels, bool noFilter)
	: width(width),
	height(height),
	pixels(pixels),
	texture(0)
{
	//Create texture
	glGenTextures(1, &this->texture);
	glBindTexture(GL_TEXTURE_2D, this->texture);
	//Set texture parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, noFilter ? GL_NEAREST : GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, noFilter ? GL_NEAREST : GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	//Create texture
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, this->width, this->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, this->pixels.data());
	//Unbind texture
	glBindTexture(GL_TEXTURE_2D, 0);
}


Texture::~Texture()
{
}

GLuint Texture::GetTexture() {
	return this->texture;
}

void Texture::Destroy() {
	glDeleteTextures(1, &this->texture);
}

unsigned int Texture::GetWidth() {
	return this->width;
}

unsigned int Texture::GetHeight() {
	return this->height;
}

void Texture::CreateTexture(std::string name, unsigned int width, unsigned int height, std::vector<unsigned char> pixels, bool noFilter) {
	if (Texture::cache.find(name) != Texture::cache.end())
		Texture::cache[name].Destroy();
	Texture::cache[name] = Texture(width, height, pixels, noFilter);
}

Texture Texture::FindTexture(std::string name) {
	if (Texture::cache.find(name) == Texture::cache.end()) {
		std::cout << "Error: Could not find Texture named " << name.c_str() << ".\n";
		return Texture();
	}
	return Texture::cache[name];
}

void Texture::DeleteTexture(std::string name) {
	if (Texture::cache.find(name) == Texture::cache.end())
		return;
	Texture::cache[name].Destroy();
}

void Texture::ClearCache() {
	std::map<std::string, Texture>::iterator it;
	for (it = Texture::cache.begin(); it != Texture::cache.end(); it++)
		it->second.Destroy();
}
