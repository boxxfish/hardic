#pragma once

#include <engine/Time.h>
#include <engine/Input.h>
#include <eventsystem/EventQueue.h>
#include <engine/Resources.h>
#include <engine/Camera.h>

struct AppSettings {
	unsigned int windowWidth, windowHeight;
};

class Services
{
public:
	Services();
	~Services();

	Time time;
	Input input;
	EventQueue sysEventQueue;
	Resources resources;
	Camera* camera;
	AppSettings appSettings;
};

