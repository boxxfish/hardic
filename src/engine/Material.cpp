#include "stdafx.h"
#include "Material.h"

std::map <std::string, float> Material::floatGlobalUniforms;
std::map <std::string, glm::vec2> Material::vec2GlobalUniforms;
std::map <std::string, glm::vec3> Material::vec3GlobalUniforms;
std::map <std::string, glm::vec4> Material::vec4GlobalUniforms;
std::map <std::string, glm::mat4> Material::mat4GlobalUniforms;

Material::Material() 
{}

Material::Material(Shader shader) 
	:shader(shader)
{}

Material::~Material()
{
}

GLuint Material::GetProgram() {
	return this->shader.GetProgram();
}

void Material::SetUniforms() {
	//Set global uniforms
	std::map<std::string, float>::iterator gFloatIt;
	for (gFloatIt = Material::floatGlobalUniforms.begin(); gFloatIt != Material::floatGlobalUniforms.end(); gFloatIt++) {
		GLint loc = glGetUniformLocation(this->shader.GetProgram(), gFloatIt->first.c_str());
		glUniform1f(loc, gFloatIt->second);
	}
	std::map < std::string, glm::vec2> ::iterator gVec2It;
	for (gVec2It = Material::vec2GlobalUniforms.begin(); gVec2It != Material::vec2GlobalUniforms.end(); gVec2It++) {
		GLint loc = glGetUniformLocation(this->shader.GetProgram(), gVec2It->first.c_str());
		glUniform2fv(loc, 1, glm::value_ptr(gVec2It->second));
	}
	std::map < std::string, glm::vec3> ::iterator gVec3It;
	for (gVec3It = Material::vec3GlobalUniforms.begin(); gVec3It != Material::vec3GlobalUniforms.end(); gVec3It++) {
		GLint loc = glGetUniformLocation(this->shader.GetProgram(), gVec3It->first.c_str());
		glUniform3fv(loc, 1, glm::value_ptr(gVec3It->second));
	}
	std::map < std::string, glm::vec4> ::iterator gVec4It;
	for (gVec4It = Material::vec4GlobalUniforms.begin(); gVec4It != Material::vec4GlobalUniforms.end(); gVec4It++) {
		GLint loc = glGetUniformLocation(this->shader.GetProgram(), gVec4It->first.c_str());
		glUniform4fv(loc, 1, glm::value_ptr(gVec4It->second));
	}
	std::map < std::string, glm::mat4> ::iterator gMat4It;
	for (gMat4It = Material::mat4GlobalUniforms.begin(); gMat4It != Material::mat4GlobalUniforms.end(); gMat4It++) {
		GLint loc = glGetUniformLocation(this->shader.GetProgram(), gMat4It->first.c_str());
		glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(gMat4It->second));
	}
	//Set uniforms
	for (gFloatIt = this->floatUniforms.begin(); gFloatIt != this->floatUniforms.end(); gFloatIt++) {
		GLint loc = glGetUniformLocation(this->shader.GetProgram(), gFloatIt->first.c_str());
		glUniform1f(loc, gFloatIt->second);
	}
	for (gVec2It = this->vec2Uniforms.begin(); gVec2It != this->vec2Uniforms.end(); gVec2It++) {
		GLint loc = glGetUniformLocation(this->shader.GetProgram(), gVec2It->first.c_str());
		glUniform2fv(loc, 1, glm::value_ptr(gVec2It->second));
	}
	for (gVec3It = this->vec3Uniforms.begin(); gVec3It != this->vec3Uniforms.end(); gVec3It++) {
		GLint loc = glGetUniformLocation(this->shader.GetProgram(), gVec3It->first.c_str());
		glUniform3fv(loc, 1, glm::value_ptr(gVec3It->second));
	}
	for (gVec4It = this->vec4Uniforms.begin(); gVec4It != this->vec4Uniforms.end(); gVec4It++) {
		GLint loc = glGetUniformLocation(this->shader.GetProgram(), gVec4It->first.c_str());
		glUniform4fv(loc, 1, glm::value_ptr(gVec4It->second));
	}
	for (gMat4It = this->mat4Uniforms.begin(); gMat4It != this->mat4Uniforms.end(); gMat4It++) {
		GLint loc = glGetUniformLocation(this->shader.GetProgram(), gMat4It->first.c_str());
		glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(gMat4It->second));
	}
	std::map<std::string, int>::iterator intIt;
	for (intIt = this->intUniforms.begin(); intIt != this->intUniforms.end(); intIt++) {
		GLint loc = glGetUniformLocation(this->shader.GetProgram(), intIt->first.c_str());
		glUniform1i(loc, intIt->second);
	}

	//Activate textures
	for (int i = 0; i < 8; i++) {
		if (this->units[i]) {
			glActiveTexture(GL_TEXTURE0 + i);
			glBindTexture(GL_TEXTURE_2D, this->textures[i].GetTexture());
		}
	}
}

void Material::SetUniform(std::string name, float value) {
	this->floatUniforms[name] = value;
}

void Material::SetUniform(std::string name, glm::vec2 value) {
	this->vec2Uniforms[name] = value;
}

void Material::SetUniform(std::string name, glm::vec3 value) {
	this->vec3Uniforms[name] = value;
}

void Material::SetUniform(std::string name, glm::vec4 value) {
	this->vec4Uniforms[name] = value;
}

void Material::SetUniform(std::string name, glm::mat4 value) {
	this->mat4Uniforms[name] = value;
}

void Material::SetGlobalUniform(std::string name, float value) {
	Material::floatGlobalUniforms[name] = value;
}

void Material::SetGlobalUniform(std::string name, glm::vec2 value) {
	Material::vec2GlobalUniforms[name] = value;
}

void Material::SetGlobalUniform(std::string name, glm::vec3 value) {
	Material::vec3GlobalUniforms[name] = value;
}

void Material::SetGlobalUniform(std::string name, glm::vec4 value) {
	Material::vec4GlobalUniforms[name] = value;
}

void Material::SetGlobalUniform(std::string name, glm::mat4 value) {
	Material::mat4GlobalUniforms[name] = value;
}

void Material::SetTexture(std::string name, Texture texture, int texUnit) {
	this->intUniforms[name] = texUnit;
	this->textures[texUnit] = texture;
	this->units[texUnit] = true;
}
