#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>

#include "Transform.h"
#include <engine/Camera.h>

class PerspCamera : public Camera
{
public:
	PerspCamera();
	PerspCamera(int width, int height, float fov);
	~PerspCamera();

	///<summary>
	///Returns projection matrix.
	///</summary>
	glm::mat4 GetProjection();

	///<summary>
	///Returns window width.
	///</summary>
	int GetWindowWidth();

	///<summary>
	///Returns window height.
	///</summary>
	int GetWindowHeight();

private:
	float aspect;
	int width, height;
};

