#pragma once
class Time
{
public:
	///<summary>
	///Returns delta time for this frame.
	///</summary>
	float GetDeltaTime();

	///<summary>
	///Returns current time in seconds since start of game.
	///</summary>
	float GetTime();

	void SetTime(float time);
private:
	float currTime, lastTime;
};

