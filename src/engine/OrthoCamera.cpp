#include "stdafx.h"
#include "OrthoCamera.h"


OrthoCamera::OrthoCamera() : Camera()
{
}

OrthoCamera::OrthoCamera(int width, int height, float size) : Camera() {
	this->aspect = (float)width / height;
	this->width = width;
	this->height = height;
	this->size = size;

	//Generate projection
	this->projection = glm::ortho(aspect * size, -aspect * size, size, -size);
}


OrthoCamera::~OrthoCamera()
{
}

glm::mat4 OrthoCamera::GetProjection() {
	return this->projection;
}

void OrthoCamera::SetSize(float size) {
	this->size = size;
	this->projection = glm::ortho(aspect * size, -aspect * size, size, -size);
}

float OrthoCamera::GetSize() {
	return this->size;
}

int OrthoCamera::GetWindowWidth() {
	return this->width;
}

int OrthoCamera::GetWindowHeight() {
	return this->height;
}

glm::vec2 OrthoCamera::ScreenToWorld(glm::vec2 screenPoint) {
	return glm::vec2(glm::inverse(this->view.GetLocalMatrix()) * glm::inverse(this->GetProjection()) * glm::vec4((screenPoint.x / this->GetWindowWidth()) * 2 - 1, (screenPoint.y / this->GetWindowHeight()) * -2 + 1, 0, 1));
}

glm::vec2 OrthoCamera::ScreenProject(glm::vec2 screenPoint) {
	return glm::vec2(glm::inverse(this->GetProjection()) * glm::vec4((screenPoint.x / this->GetWindowWidth()) * 2 - 1, (screenPoint.y / this->GetWindowHeight()) * -2 + 1, 0, 1));
}
