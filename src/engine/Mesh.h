#pragma once

#define GLEW_STATIC
#include <gl/glew.h>

#include <vector>

class Mesh
{
public:
	Mesh();
	///<summary>
	///Default format is vertex (3) and texCoords (2).
	///</summary>
	Mesh(std::vector<float> coords, bool useDefault = true);
	~Mesh();

	Mesh(const Mesh& other);
	Mesh& operator=(const Mesh& other);

	///<summary>
	///Returns coordinates.
	///</summary>
	std::vector<float> GetCoords();

	///<summary>
	///Sets up VAO and creates the mesh.
	///</summary>
	void Create();

	///<summary>
	///Returns number of coords.
	///</summary>
	int GetCoordCount();

	///<summary>
	///Adds vertex attribute.
	///</summary>
	void AddVertexAttrib(int numEntries, bool isFloat);

	///<summary>
	///Returns VAO.
	///</summary>
	GLuint GetVAO();
private:
	struct VertexAttrib {
		int numEntries;
		bool isFloat;
	};

	int entryCount;
	std::vector<float> coords;
	std::vector<VertexAttrib> vertexAttribs;
	GLuint VBO, VAO;
	unsigned int* refCount;
};

