#pragma once

#define GLEW_STATIC
#include <gl/glew.h>

#include <map>
#include <iostream>
#include <string>

class Shader
{
public:
	Shader();
	Shader(std::string vSrc, std::string fSrc);
	~Shader();

	///<summary>
	///Returns program.
	///</summary>
	GLuint GetProgram();

	///<summary>
	///Destroys shader.
	///</summary>
	void Destroy();

	///<summary>
	///Creates shader and stores it in the cache.
	///</summary>
	static void CreateShader(std::string name, std::string vSrc, std::string fSrc);

	///<summary>
	///Returns specified shader.
	///</summary>
	static Shader FindShader(std::string name);

	///<summary>
	///Deletes shader from cache.
	///</summary>
	static void DeleteShader(std::string name);

	///<summary>
	///Clears cache.
	///</summary>
	static void ClearCache();
private:
	GLuint program;
	static std::map<std::string, Shader> cache;
};

