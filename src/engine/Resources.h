#pragma once

#include <zlib/zlib.h>
#include <libpng/png.h>

#include <iostream>
#include <fstream>
#include <experimental/filesystem>

#include "Shader.h"
#include "TreeNode.h"
#include "Texture.h"
#include <Utilities.h>

namespace fs = std::experimental::filesystem;

class Resources
{
public:
	struct FileData {
		std::vector<char> data;
		bool isFile;
	};

	///<summary>
	///Loads resources into memory from archive file.
	///</summary>
	void LoadResourcesFromFile(std::string path);

	///<summary>
	///Loads resources into memory from folder.
	///</summary>
	void LoadResourcesFromFolder(std::string path);

	///<summary>
	///Returns resource node at path. Path should be in the form of "folder1/folder2/item".
	///</summary>
	std::shared_ptr<TreeNode<FileData>> GetResNode(std::string path);

private:
	void LoadFileTreeFromFolder(TreeNode<FileData>* currNode, fs::directory_iterator currPath);
	void SetUp();

	TreeNode<FileData> fileTreeRootNode;

	static void DecodePNG(std::vector<char> data, unsigned char* colorType, unsigned int* width, unsigned int* height, std::vector<unsigned char>* pixels);
	static void ReadPNGFromMemory(png_structp png_ptr, png_bytep output, png_size_t byteCount);

	static unsigned int streamCount;
};

