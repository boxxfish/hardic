#include "stdafx.h"
#include "Scene.h"

Scene::Scene() {
	this->initialized = false;
}


Scene::~Scene()
{
}

Services* Scene::GetServices() {
	return this->services;
}

EventQueue* Scene::GetEventQueue() {
	return &this->sceneEventQueue;
}

void Scene::AddGameObject(GameObject * g)
{
	//Add gameobject to vector
	this->gameObjects.push_back(g);
	//Set gameobject's services
	g->SetServices(this->services);
	//Set gameobject's scene event queue
	g->SetSceneEventQueue(&this->sceneEventQueue);
	//Add to root node
	this->rootNode.AddChild(g->GetTransform());
	//Start game object if the scene is already active
	if (this->initialized)
		g->Start();
}

void Scene::AddGameObject(GameObject* g, std::string parentName) {
	//Add gameobject to vector
	this->gameObjects.push_back(g);
	//Set gameobject's services
	g->SetServices(this->services);
	//Set gameobject's scene event queue
	g->SetSceneEventQueue(&this->sceneEventQueue);
	//Find parent and add gameobject's transform to it as a child
	bool foundParent = false;
	for (int i = 0; i < this->gameObjects.size(); i++) {
		if (this->gameObjects[i]->GetTransform()->name == parentName) {
			this->gameObjects[i]->GetTransform()->AddChild(this->gameObjects[this->gameObjects.size() - 1]->GetTransform());
			foundParent = true;
		}
	}
	if (!foundParent)
		std::cout << "Error: Could not find game object " << g->GetTransform()->name << " in scene.\n";
	//Start game object if the scene is already active
	if (this->initialized)
		g->Start();
}

void Scene::AddGameObject(GameObject* g, GameObject* parent) {
	//Add gameobject to vector
	this->gameObjects.push_back(g);
	//Set gameobject's services
	g->SetServices(this->services);
	//Set gameobject's scene event queue
	g->SetSceneEventQueue(&this->sceneEventQueue);
	//Find parent and add gameobject's transform to it as a child
	bool foundParent = false;
	for (int i = 0; i < this->gameObjects.size(); i++) {
		if (this->gameObjects[i]->GetTransform() == parent->GetTransform()) {
			this->gameObjects[i]->GetTransform()->AddChild(this->gameObjects[this->gameObjects.size() - 1]->GetTransform());
			foundParent = true;
		}
	}
	if (!foundParent)
		std::cout << "Error: Could not find game object " << g->GetTransform()->name << " in scene.\n";
	//Start game object if the scene is already active
	if (this->initialized)
		g->Start();
}

void Scene::RemoveGameObject(GameObject* g) {
	for (int i = 0; i < this->gameObjects.size(); i++) {
		if (this->gameObjects[i] == g) {
			//Unlink gameobject from transform tree
			this->RemoveTransform(this->gameObjects[i]->GetTransform(), &rootNode);
			//Remove gameobject from vector
			this->gameObjects.erase(this->gameObjects.begin() + i);
			return;
		}
	}
	std::cout << "Error: Could not find gameobject " << g->GetTransform()->name << " in scene.\n";
}

void Scene::Init() {
	//Set scene root node name
	this->rootNode.name = "rootNode";
	//Register scene event listener
	this->sceneEventQueue.RegisterListener((EventListener*)&this->eventListener);
	//Set event queue's event listener
	this->eventListener.gameObjects = &this->gameObjects;
	//Start scene
	this->Start();
	//Set init flag to true
	this->initialized = true;
	//Start gameobjects
	for (int i = 0; i < this->gameObjects.size(); i++)
		this->gameObjects[i]->Start();
	//Display scene info
	this->Display();
}

void Scene::Start() {}

void Scene::UpdateScene() {
	//Clear graphics cache
	this->graphicsCache.clear();
	//Update scene
	this->Update();
	//Update gameobjects
	for (int i = 0; i < this->gameObjects.size(); i++)
		this->gameObjects[i]->Update();
	//Propagate transforms
	PropagateTransform(&this->rootNode);
	//Late update gameobjects
	for (int i = 0; i < this->gameObjects.size(); i++)
		this->gameObjects[i]->LateUpdate();
	//Update graphics
	for (int i = 0; i < this->gameObjects.size(); i++) {
		if (this->gameObjects[i]->UseGraphic()) {
			this->gameObjects[i]->GetGraphic()->Update();
			this->graphicsCache.push_back(this->gameObjects[i]->GetGraphic()->renderObject);
		}
	}
	//Display transform tree if ctrl-t
	if (this->services->input.GetKey(GLFW_KEY_LEFT_CONTROL) && this->services->input.GetKeyDown(GLFW_KEY_T))
		this->Display();
}

void Scene::Update() {}

std::vector<RenderObject> Scene::GetGraphicsCache() {
	return this->graphicsCache;
}

void Scene::Display() {
	this->rootNode.Display();
}

void Scene::SetServices(Services* services) {
	this->services = services;
}

void Scene::PropagateTransform(TreeNode<Transform>* node) {
	for (int i = 0; i < node->GetChildCount(); i++) {
		node->GetChild(i)->data.SetGlobalMatrix(node->data.GetLocalMatrix());
		PropagateTransform(&*node->GetChild(i));
	}
}

void Scene::RemoveTransform(std::shared_ptr<TreeNode<Transform>> transform, TreeNode<Transform>* parent) {
	for (int i = 0; i < parent->GetChildCount(); i++) {
		if (transform == parent->GetChild(i)) {
			parent->RemoveChild(i);
			return;
		}
		RemoveTransform(transform, &*parent->GetChild(i));
	}
}

void Scene::ReplaceReference(GameObject* former, GameObject* latter) {
	for (int i = 0; i < this->gameObjects.size(); i++) {
		if (this->gameObjects[i] == former) {
			this->gameObjects[i] = latter;
			return;
		}
	}
	std::cout << "Error: Could not find reference to replace.\n";
}

GameObject* Scene::FindGameObject(std::string name) {
	for (int i = 0; i < this->gameObjects.size(); i++) {
		if (this->gameObjects[i]->GetTransform()->name == name)
			return this->gameObjects[i];
	}
	std::cout << "Error: Could not find gameobject " << name << " in scene.\n";
	return nullptr;
}
