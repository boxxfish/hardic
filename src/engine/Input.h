#pragma once

#define GLEW_STATIC
#include <gl/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>

#include <assert.h>

class Input
{
public:
	///<summary>
	///Is the key down?
	///</summary>
	bool GetKey(int key);

	///<summary>
	///Has the key been pressed this frame?
	///</summary>
	bool GetKeyDown(int key);

	///<summary>
	///Has the key been released this frame?
	///</summary>
	bool GetKeyUp(int key);

	///<summary>
	///Is the mouse button down?
	///</summary>
	bool GetMouseButton(int button);

	///<summary>
	///Has the mouse button been pressed this frame?
	///</summary>
	bool GetMouseButtonDown(int button);

	///<summary>
	///Has the mouse button been released this frame?
	///</summary>
	bool GetMouseButtonUp(int button);

	///<summary>
	///Returns cursor's x position.
	///</summary>
	float GetCursorXPos();

	///<summary>
	///Returns cursor's y position.
	///</summary>
	float GetCursorYPos();

	///<summary>
	///Returns true if file has been dropped this frame.
	///</summary>
	bool IsFileDropped();

	///<summary>
	///Returns number of files dropped this frame.
	///</summary>
	unsigned short int GetFileCount();

	///<summary>
	///Returns the path to a file.
	///</summary>
	std::string GetFilePath(int index);

	///<summary>
	///Updates Input.
	///</summary>
	void Update();

	///<summary>
	///Handler for keyboard callback.
	///</summary>
	static void KeyboardHandler(GLFWwindow* window, int key, int scancode, int action, int mods);

	///<summary>
	///Handler for mouse button callback.
	///</summary>
	static void MouseButtonHandler(GLFWwindow* window, int button, int action, int mods);

	///<summary>
	///Handler for cursor position callback.
	///</summary>
	static void CursorPositionHandler(GLFWwindow* window, double xpos, double ypos);

	///<summary>
	///Handler for file drop callback.
	///</summary>
	static void FileDropHandler(GLFWwindow* window, int fileCount, char** paths);
private:
	static bool keys[512];
	static bool lastKeys[512];
	static bool mButtons[3];
	static bool lastMButtons[3];
	static float cursorXpos;
	static float cursorYpos;
	static std::string filePaths[10];
	static unsigned short int fileCount;
};