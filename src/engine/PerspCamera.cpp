#include "stdafx.h"
#include "PerspCamera.h"

PerspCamera::PerspCamera() : Camera()
{
}

PerspCamera::PerspCamera(int width, int height, float fov) : Camera()
{
	this->width = width;
	this->height = height;
	this->aspect = width / (float)height;

	//Generate projection
	this->projection = glm::perspectiveFov<float>(fov, this->width, this->height, 0.1f, 1000.0f);
}

PerspCamera::~PerspCamera()
{
}

glm::mat4 PerspCamera::GetProjection()
{
	return this->projection;
}

int PerspCamera::GetWindowWidth()
{
	return this->width;
}

int PerspCamera::GetWindowHeight()
{
	return height;
}
