#pragma once

#include <map>

#include "Scene.h"
#include <eventsystem/SceneManagerEventListener.h>

class SceneManager
{
public:
	SceneManager();
	~SceneManager();

	///<summary>
	///Adds new scene and stores it in the cache.
	///</summary>
	void AddScene(Scene* scene);

	///<summary>
	///Sets active scene and initializes it.
	///</summary>
	void SetActiveScene(std::string name);

	///<summary>
	///Returns active scene.
	///</summary>
	Scene* GetActiveScene();

	///<summary>
	///Sets pointer to service struct.
	///</summary>
	void SetServices(Services* services);

	///<summary>
	///Returns pointer to service struct.
	///</summary>
	Services* GetServices();

	///<summary>
	///Returns pointer to internal event listener.
	///</summary>
	SceneManagerEventListener* GetListener();
private:
	std::map<std::string, Scene*> scenes;
	Scene* activeScene;
	Services* services;
	SceneManagerEventListener eventListener;
};

