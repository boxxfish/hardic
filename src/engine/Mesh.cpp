#include "stdafx.h"
#include "Mesh.h"

Mesh::Mesh() {
	this->entryCount = 0;
	this->refCount = new unsigned int(0);
}


Mesh::Mesh(std::vector<float> coords, bool useDefault) {
	this->entryCount = 0;
	this->refCount = new unsigned int(0);
	//Set coords
	this->coords = coords;

	//Set default vertex attribs
	if (useDefault) {
		this->AddVertexAttrib(3, true);
		this->AddVertexAttrib(2, true);
		this->Create();
	}
}

Mesh::~Mesh() {
	if ((*refCount)-- == 0) {
		glDeleteBuffers(1, &this->VBO);
		glDeleteVertexArrays(1, &this->VAO);
		delete this->refCount;
	}
}

Mesh::Mesh(const Mesh& other) {
	*this = other;
}

Mesh& Mesh::operator=(const Mesh& other) {
	if (this != &other) {
		this->coords = other.coords;
		this->entryCount = other.entryCount;
		this->VAO = other.VAO;
		this->VBO = other.VAO;
		this->vertexAttribs = other.vertexAttribs;
		this->refCount = other.refCount;
		(*this->refCount)++;
	}
	return *this;
}


std::vector<float> Mesh::GetCoords() {
	return this->coords;
}

void Mesh::Create() {
	//Create VBO and VAO
	glGenBuffers(1, &this->VBO);
	glGenVertexArrays(1, &this->VAO);

	glBindVertexArray(this->VAO);

	//Fill vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
	glBufferData(GL_ARRAY_BUFFER, this->coords.size() * sizeof(GL_FLOAT), this->coords.data(), GL_STATIC_DRAW);

	//Set vertex attributes
	int stride = 0;
	for (int i = 0; i < this->vertexAttribs.size(); i++)
		stride += this->vertexAttribs[i].numEntries * (this->vertexAttribs[i].isFloat ? sizeof(GL_FLOAT) : sizeof(GL_INT));
	int floatCount = 0, intCount = 0;
	for (int i = 0; i < this->vertexAttribs.size(); i++) {
		if (this->vertexAttribs[i].isFloat) {
			glVertexAttribPointer(i, this->vertexAttribs[i].numEntries, GL_FLOAT, GL_FALSE, stride, (GLvoid*)(floatCount * sizeof(GLfloat) + intCount * sizeof(GLint)));
			floatCount += this->vertexAttribs[i].numEntries;
		}
		else {
			glVertexAttribPointer(i, this->vertexAttribs[i].numEntries, GL_INT, GL_FALSE, stride, (GLvoid*)(floatCount * sizeof(GLfloat) + intCount * sizeof(GLint)));
			intCount += this->vertexAttribs[i].numEntries;
		}
		glEnableVertexAttribArray(i);
	}

	glBindVertexArray(0);
}

int Mesh::GetCoordCount() {
	return this->coords.size() / this->entryCount;
}

void Mesh::AddVertexAttrib(int numEntries, bool isFloat) {
	VertexAttrib attrib;
	attrib.numEntries = numEntries;
	attrib.isFloat = isFloat;
	this->vertexAttribs.push_back(attrib);
	this->entryCount += numEntries;
}

GLuint Mesh::GetVAO() {
	return this->VAO;
}