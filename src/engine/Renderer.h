#pragma once

#define GLEW_STATIC
#include <gl/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include <iostream>
#include <vector>

#include "Material.h"
#include "Mesh.h"
#include "Services.h"

struct RenderObject {
	Mesh mesh;
	Material material;
	unsigned int order;
	RenderObject()
		: order(0) {};
	RenderObject(Mesh mesh, Material material)
		: mesh(mesh), material(material), order(0) {};
};

class Renderer
{
public:
	Renderer();
	~Renderer();

	///<summary>
	///Initializes the renderer.
	///</summary>
	void Init(int width, int height, std::string title, Services* services);

	///<summary>
	///Renders cached renderobjects.
	///</summary>
	void Render(bool swapBuffers = true);

	///<summary>
	///Should the window close?
	///</summary>
	bool GetWindowClose();

	///<summary>
	///Sets the clear color.
	///</summary>
	void SetClearColor(glm::vec3 clearColor);

	///<summary>
	///Adds renderobject.
	///</summary>
	void AddRenderObject(RenderObject obj);

	///<summary>
	///Adds vector of renderobjects.
	///</summary>
	void AddRenderObjects(std::vector<RenderObject> vector);

	///<summary>
	///Replaces renderer's renderobject vector.
	///</summary>
	void SetRenderObjects(std::vector<RenderObject> vector);

	///<summary>
	///Polls input.
	///</summary>
	void PollInput();

	///<summary>
	///Clears buffers.
	///</summary>
	void ClearBuffers();

	///<summary>
	///Returns window width.
	///</summary>
	int GetWidth();

	///<summary>
	///Returns window height.
	///</summary>
	int GetHeight();

	///<summary>
	///Returns window aspect ratio.
	///</summary>
	float GetAspectRatio();
private:
	GLFWwindow* window;
	int width, height;
	glm::vec3 clearColor;
	std::vector<RenderObject> renderObjects;
	Services* services;
};

