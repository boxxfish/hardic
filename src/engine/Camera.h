#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>

#include "Transform.h"

class Camera
{
public:
	Camera();
	~Camera();

	///<summary>
	///Returns projection matrix.
	///</summary>
	virtual glm::mat4 GetProjection() = 0;

	///<summary>
	///Returns window width.
	///</summary>
	virtual int GetWindowWidth() = 0;

	///<summary>
	///Returns window height.
	///</summary>
	virtual int GetWindowHeight() = 0;

	Transform view;
protected:
	glm::mat4 projection;
};

