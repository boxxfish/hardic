#include "stdafx.h"
#include "Resources.h"

unsigned int Resources::streamCount;

void Resources::LoadResourcesFromFile(std::string path) {
	//Set up
	this->SetUp();
}

void Resources::LoadResourcesFromFolder(std::string path) {
	//Open folder
	if (!fs::exists(path)) {
		std::cout << "Error: No such folder exists at location.\n";
		return;
	}
	//Fill file tree
	this->LoadFileTreeFromFolder(&this->fileTreeRootNode, fs::directory_iterator(path));
	//Set up
	this->SetUp();
}

std::shared_ptr<TreeNode<Resources::FileData>> Resources::GetResNode(std::string path) {
	//Split path by '/'
	std::vector<std::string> parts = Utilities::Split(path, '/');
	//Iterate over parts and find the corresponding node for each
	std::shared_ptr<TreeNode<FileData>> currNode = this->fileTreeRootNode.FindChild(parts[0]);
	for (int i = 1; i < parts.size(); i++) {
		currNode = currNode->FindChild(parts[i]);
	}
	//Return last node found
	return currNode;
}

void Resources::LoadFileTreeFromFolder(TreeNode<FileData>* currNode, fs::directory_iterator currPath) {
	//Iterate over directory members
	std::experimental::filesystem::directory_iterator it;
	for (it = fs::begin(currPath); it != fs::end(currPath); it++) {
		//Create child node
		std::shared_ptr<TreeNode<FileData>> childPtr = currNode->CreateChild();
		childPtr->name = it->path().filename().string();
		//If child is parent, repeat. If not, load file.
		if (fs::is_directory(it->path())) {
			childPtr->data.isFile = false;
			//Repeat
			this->LoadFileTreeFromFolder(&(*childPtr), fs::directory_iterator(it->path()));
		}
		else {
			childPtr->data.isFile = true;
			//Load file
			std::ifstream file(it->path(), std::ios::binary);
			childPtr->data.data = std::vector<char>(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>());
			file.close();
		}
	}
}

void Resources::SetUp() {
	//Cache shaders
	std::shared_ptr<TreeNode<FileData>> shaderFiles = this->fileTreeRootNode.FindChild("shaders");
	for (int i = 0; i < shaderFiles->GetChildCount(); i++) {
		//Only check vertex shaders
		std::string fileName = shaderFiles->GetChild(i)->name;
		if (fileName.substr(fileName.size() - 5) == ".vert" && shaderFiles->GetChild(i)->data.isFile) {
			std::string shaderName = fileName.substr(0, fileName.size() - 5);
			std::string vSrc = std::string(shaderFiles->GetChild(i)->data.data.begin(), shaderFiles->GetChild(i)->data.data.end());
			std::shared_ptr<TreeNode<FileData>> fSrcNode = this->fileTreeRootNode.FindChild("shaders")->FindChild(shaderName + ".frag");
			std::string fSrc = std::string(fSrcNode->data.data.begin(), fSrcNode->data.data.end());
			Shader::CreateShader(shaderName, vSrc, fSrc);
		}
	}
	//Cache textures
	std::shared_ptr<TreeNode<FileData>> textureFiles = this->fileTreeRootNode.FindChild("textures");
	for (int i = 0; i < textureFiles->GetChildCount(); i++) {
		if (textureFiles->GetChild(i)->data.isFile) {
			std::vector<unsigned char> pixels;
			unsigned int width, height;
			unsigned char colorType;
			Resources::DecodePNG(textureFiles->GetChild(i)->data.data, &colorType, &width, &height, &pixels);
			Texture::CreateTexture(textureFiles->GetChild(i)->name.substr(0, textureFiles->GetChild(i)->name.size() - 4), width, height, pixels, false);
		}
	}
	Texture::CreateTexture("white", 1, 1, std::vector<unsigned char>() = { 255, 255, 255, 255 });
	Texture::CreateTexture("black", 1, 1, std::vector<unsigned char>() = { 0, 0, 0, 255 });
}

void Resources::DecodePNG(std::vector<char> data, unsigned char* colorType, unsigned int* width, unsigned int* height, std::vector<unsigned char>* pixels) {
	//Check if PNG
	std::vector<char> header;
	for (int i = 0; i < 8; i++)
		header.push_back(data[i]);
	if (png_sig_cmp((png_const_bytep)header.data(), 0, 8)) {
		std::cout << "Error: Data passed was not a PNG file.\n";
		return;
	}

	//Create structs
	png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png_ptr) {
		std::cout << "Error: PNG read struct could not be created.\n";
		return;
	}
	png_infop png_info_ptr = png_create_info_struct(png_ptr);
	if (!png_info_ptr) {
		std::cout << "Error: PNG info struct could not be created.\n";
		png_destroy_read_struct(&png_ptr, NULL, NULL);
		return;
	}
	//Set png_ptr to file data
	png_set_read_fn(png_ptr, &data, ReadPNGFromMemory);

	//Read header
	png_read_info(png_ptr, png_info_ptr);
	*width = png_get_image_width(png_ptr, png_info_ptr);
	*height = png_get_image_height(png_ptr, png_info_ptr);
	*colorType = png_get_color_type(png_ptr, png_info_ptr);

	//Check if valid color type
	if (*colorType != PNG_COLOR_TYPE_RGBA) {
		std::cout << "Error: PNG file was not RGBA.\n";
		return;
	}

	//Read data
	png_bytep* row_ptrs = new png_bytep[*height];
	for (int i = 0; i < *height; i++)
		row_ptrs[i] = new png_byte[png_get_rowbytes(png_ptr, png_info_ptr)];
	png_read_image(png_ptr, row_ptrs);
	std::vector<unsigned char> pix;
	for (int y = 0; y < *height; y++) {
		for (int x = 0; x < *width; x++) {
			pix.push_back(row_ptrs[y][(*width - x - 1) * 4]);
			pix.push_back(row_ptrs[y][(*width - x - 1) * 4 + 1]);
			pix.push_back(row_ptrs[y][(*width - x - 1) * 4 + 2]);
			pix.push_back(row_ptrs[y][(*width - x - 1) * 4 + 3]);
		}
		delete[] row_ptrs[y];
	}
	delete[] row_ptrs;
	*pixels = pix;

	//Cleanup
	png_destroy_info_struct(png_ptr, &png_info_ptr);
	png_destroy_read_struct(&png_ptr, NULL, NULL);
	Resources::streamCount = 0;
}

void Resources::ReadPNGFromMemory(png_structp png_ptr, png_bytep output, png_size_t byteCount) {
	std::vector<char> data(*((std::vector<char>*)png_get_io_ptr(png_ptr)));
	memcpy(output, data.data() + Resources::streamCount, byteCount);
	Resources::streamCount += byteCount;
}