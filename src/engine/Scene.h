#pragma once

#include <iostream>
#include <vector>
#include <list>

#include "GameObject.h"
#include "TreeNode.h"
#include "Services.h"
#include <eventsystem/SceneEventListener.h>

class Scene
{
public:
	Scene();
	~Scene();

	///<summary>
	///Initializes scene.
	///</summary>
	void Init();

	///<summary>
	///Starts scene.
	///</summary>
	virtual void Start();

	///<summary>
	///Updates scene.
	///</summary>
	void UpdateScene();

	///<summary>
	///Updates scene.
	///</summary>
	virtual void Update();

	///<summary>
	///Returns graphics cache.
	///</summary>
	std::vector<RenderObject> GetGraphicsCache();

	///<summary>
	///Outputs scene information.
	///</summary>
	void Display();

	///<summary>
	///Sets pointer to service struct.
	///</summary>
	void SetServices(Services* services);

	///<summary>
	///Returns scene's event queue.
	///</summary>
	EventQueue* GetEventQueue();

	void AddGameObject(GameObject* g);
	void AddGameObject(GameObject* g, std::string parentName);
	void AddGameObject(GameObject* g, GameObject* parent);
	void RemoveGameObject(GameObject* g);
	GameObject* FindGameObject(std::string name);

	std::string name;
protected:
	Services* GetServices();
	TreeNode<Transform> rootNode;
private:
	void PropagateTransform(TreeNode<Transform>* node);
	void RemoveTransform(std::shared_ptr<TreeNode<Transform>> target, TreeNode<Transform>* parent);
	void ReplaceReference(GameObject* former, GameObject* latter);

	std::vector<GameObject*> gameObjects;
	std::vector<RenderObject> graphicsCache;
	bool initialized;
	Services* services;
	EventQueue sceneEventQueue;
	SceneEventListener eventListener;
};