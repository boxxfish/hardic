#pragma once

#define GLEW_STATIC
#include <gl/glew.h>

#include <vector>
#include <map>
#include <iostream>

class Texture
{
public:
	Texture();
	Texture(unsigned int width, unsigned int height, std::vector<unsigned char> pixels, bool noFilter = true);
	~Texture();

	///<summary>
	///Returns texture.
	///</summary>
	GLuint GetTexture();

	///<summary>
	///Destroys texture.
	///</summary>
	void Destroy();

	///<summary>
	///Returns texture width.
	///</summary>
	unsigned int GetWidth();

	///<summary>
	///Returns texture height.
	///</summary>
	unsigned int GetHeight();

	///<summary>
	///Creates texture and stores it in the cache.
	///</summary>
	static void CreateTexture(std::string name, unsigned int width, unsigned int height, std::vector<unsigned char> pixels, bool noFilter = true);

	///<summary>
	///Returns specified texture.
	///</summary>
	static Texture FindTexture(std::string name);
	
	///<summary>
	///Deletes texture from cache.
	///</summary>
	static void DeleteTexture(std::string name);
	
	///<summary>
	///Clears texture cache.
	///</summary>
	static void ClearCache();
private:
	unsigned int width;
	unsigned int height;
	std::vector<unsigned char> pixels;
	GLuint texture;

	static std::map<std::string, Texture> cache;
};