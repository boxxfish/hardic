#pragma once

#include "Graphic.h"
#include "Texture.h"
#include "TreeNode.h"
#include "Transform.h"
#include "SpriteSheet.h"

class SpriteGraphic : public Graphic
{
public:
	SpriteGraphic();
	~SpriteGraphic();

	///<summary>
	///Sets sprite.
	///</summary>
	void SetSprite(SpriteSheet::Sprite sprite);

	void Update();

	glm::vec4 tint;
};

