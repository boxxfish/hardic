#pragma once

#include <engine/Graphic.h>"
#include <helpers/Font.h>

class TextGraphic : public Graphic
{
public:
	enum AlignType {
		LEFT,
		CENTER,
		RIGHT
	};

	TextGraphic();
	~TextGraphic();

	void SetFont(Font font);

	void SetTextAlign(AlignType type);

	void SetLineSpacing(float spacing);

	void SetLineWidth(float width);

	void DisplayText(std::string text);

	void Update();

	glm::vec4 tint;
private:
	Font font;
	std::string text;
	AlignType type;
	float lineSpacing;
	float lineWidth;
};

