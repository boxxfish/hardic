#include "stdafx.h"
#include "SpriteGraphic.h"


SpriteGraphic::SpriteGraphic() : Graphic() {
	this->tint = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	this->renderObject.material = Material(Shader::FindShader("sprite"));
}


SpriteGraphic::~SpriteGraphic() {
}

void SpriteGraphic::SetSprite(SpriteSheet::Sprite sprite) {
	this->renderObject.mesh = Mesh(sprite.GetCoords());
	this->renderObject.material.SetTexture("sprite", sprite.texture, 0);
}

void SpriteGraphic::Update() {
	this->renderObject.material.SetUniform("tint", this->tint);
	this->renderObject.material.SetUniform("modelMat", this->transform->data.GetGlobalMatrix());
}
