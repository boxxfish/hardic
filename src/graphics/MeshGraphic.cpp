#include "stdafx.h"
#include "MeshGraphic.h"


MeshGraphic::MeshGraphic() : Graphic() {
	this->tint = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	this->renderObject.material = Material(Shader::FindShader("basic"));
}


MeshGraphic::~MeshGraphic() {

}

void MeshGraphic::SetCoords(std::vector<float> coords) {
	Mesh mesh(coords, false);
	mesh.AddVertexAttrib(3, true);
	mesh.AddVertexAttrib(2, true);
	mesh.AddVertexAttrib(3, true);
	mesh.Create();
	this->renderObject.mesh = mesh;
}

void MeshGraphic::SetTexture(Texture tex) {
	this->renderObject.material.SetTexture("mainTex", tex, 0);
}

void MeshGraphic::Update() {
	this->renderObject.material.SetUniform("tint", this->tint);
	this->renderObject.material.SetUniform("modelMat", this->transform->data.GetGlobalMatrix());
}
