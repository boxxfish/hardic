#pragma once

#include "Graphic.h"
#include "Texture.h"

class MeshGraphic : public Graphic
{
public:
	MeshGraphic();
	~MeshGraphic();

	///<summary>
	///Sets coords.
	///</summary>
	void SetCoords(std::vector<float> coords);

	///<summary>
	///Sets texture.
	///</summary>
	void SetTexture(Texture tex);

	void Update();

	glm::vec4 tint;
};

