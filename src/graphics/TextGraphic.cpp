#include "stdafx.h"
#include "TextGraphic.h"


TextGraphic::TextGraphic() : Graphic() {
	this->renderObject.material = Material(Shader::FindShader("ui"));
	this->renderObject.mesh.AddVertexAttrib(1, true);
	this->renderObject.mesh.Create();
	this->lineSpacing = 10;
	this->lineWidth = 0;
	this->tint = glm::vec4(0, 0, 0, 1);
	this->type = AlignType::CENTER;
}


TextGraphic::~TextGraphic() {
}

void TextGraphic::SetFont(Font font) {
	this->font = font;
	this->renderObject.material.SetTexture("sprite", this->font.GetTexture(), 0);
}

void TextGraphic::SetTextAlign(AlignType type) {
	this->type = type;
}

void TextGraphic::SetLineSpacing(float spacing) {
	this->lineSpacing = spacing;
}

void TextGraphic::SetLineWidth(float width) {
	this->lineWidth = width;
}

void TextGraphic::DisplayText(std::string text) {
	this->text = text;
	//Get distance between first and last characters
	float dist = 0;
	for (int i = 0; i < this->text.size(); i++) {
		if (this->text[i] == '\n' || (this->lineWidth > 0 && std::abs(dist) >= this->lineWidth))
			break;
		dist += font.GetCharAdvance(this->text[i]).x;
	}
	//Set starting point
	float start = 0;
	if (this->type == AlignType::LEFT)
		start = -font.GetCharAdvance(this->text[0]).x;
	else if (this->type == AlignType::CENTER)
		start = dist / 2 - font.GetCharAdvance(this->text[0]).x;
	else if (this->type == AlignType::RIGHT)
		start = dist - font.GetCharAdvance(this->text[0]).x;
	//Generate coordinates
	std::vector<float> coords;
	float nextX = 0;
	float nextY = 0;
	for (int i = 0; i < this->text.size(); i++) {
		//Newline if \n or characters surpassed linewidth
		if (this->text[i] == '\n' || (this->lineWidth > 0 && std::abs(nextX) >= this->lineWidth)) {
			nextX = 0;
			nextY += this->lineSpacing;
			//Recalculate starting point
			float dist = 0;
			for (int j = i; j < this->text.size(); j++)
				dist += font.GetCharAdvance(this->text[j]).x;
			start = 0;
			if (this->type == AlignType::LEFT)
				start = -font.GetCharAdvance(this->text[i]).x;
			else if (this->type == AlignType::CENTER)
				start = dist / 2 - font.GetCharAdvance(this->text[i]).x;
			else if (this->type == AlignType::RIGHT)
				start = dist - font.GetCharAdvance(this->text[i]).x;
		}
		BoundingBox charBounds = font.GetCharBounds(this->text[i]);
		glm::vec2 charBase = font.GetCharBaseLine(this->text[i]);
		std::vector<float> charCoords = {
			//Vertices																					TexCoords
			start + nextX + charBase.x, nextY + charBounds.size.y - charBase.y, 0,						charBounds.position.x / this->font.GetTexture().GetWidth(), (charBounds.position.y + charBounds.size.y) / this->font.GetTexture().GetHeight(),
			start + nextX + charBase.x, nextY + 0 - charBase.y, 0,										charBounds.position.x / this->font.GetTexture().GetWidth(), charBounds.position.y / this->font.GetTexture().GetHeight(),
			start + nextX + charBase.x + charBounds.size.x, nextY + charBounds.size.y - charBase.y, 0,	(charBounds.position.x + charBounds.size.x) / this->font.GetTexture().GetWidth(), (charBounds.position.y + charBounds.size.y) / this->font.GetTexture().GetHeight(),

			start + nextX + charBase.x + charBounds.size.x, nextY + 0 - charBase.y, 0,					(charBounds.position.x + charBounds.size.x) / this->font.GetTexture().GetWidth(), charBounds.position.y / this->font.GetTexture().GetHeight(),
			start + nextX + charBase.x, nextY + 0 - charBase.y, 0,										charBounds.position.x / this->font.GetTexture().GetWidth(), charBounds.position.y / this->font.GetTexture().GetHeight(),
			start + nextX + charBase.x + charBounds.size.x, nextY + charBounds.size.y - charBase.y, 0,	(charBounds.position.x + charBounds.size.x) / this->font.GetTexture().GetWidth(), (charBounds.position.y + charBounds.size.y) / this->font.GetTexture().GetHeight(),
		};
		nextX -= font.GetCharAdvance(this->text[i]).x;
		coords.insert(coords.end(), charCoords.begin(), charCoords.end());
	}
	this->renderObject.mesh = Mesh(coords);
}

void TextGraphic::Update() {
	this->renderObject.material.SetUniform("tint", this->tint);
	this->renderObject.material.SetUniform("modelMat", this->transform->data.GetGlobalMatrix());
}
