#include "stdafx.h"
#include "BillboardGraphic.h"

BillboardGraphic::BillboardGraphic() : Graphic() {
	this->tint = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	this->renderObject.material = Material(Shader::FindShader("billboard"));
}

BillboardGraphic::~BillboardGraphic() {
}

void BillboardGraphic::SetSprite(SpriteSheet::Sprite sprite)
{
	Mesh mesh(sprite.GetCoords());
	this->renderObject.mesh = Mesh(sprite.GetCoords());
	this->renderObject.material.SetTexture("mainTex", sprite.texture, 0);
}

void BillboardGraphic::Update() {
	this->renderObject.material.SetUniform("tint", this->tint);
	this->renderObject.material.SetUniform("modelMat", this->transform->data.GetGlobalMatrix());
}
