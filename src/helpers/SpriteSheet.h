#pragma once

#include "Texture.h"
#include <glm/glm.hpp>

class SpriteSheet
{
public:
	struct Sprite {
		glm::vec2 size;
		glm::vec2 startTexCoord;
		glm::vec2 endTexCoord;
		Texture texture;
		Sprite() {};
		Sprite(glm::vec2 size, glm::vec2 startTexCoord, glm::vec2 endTexCoord, Texture texture)
			:size(size), startTexCoord(startTexCoord), endTexCoord(endTexCoord), texture(texture)  {};
		std::vector<float> GetCoords(glm::vec2 trans = glm::vec2(0, 0)) {
			float width = size.x / 2.0f;
			float height = size.y / 2.0f;
			std::vector<float> coords = {
				width + trans.x, -height + trans.y, 0.0f, endTexCoord.x, endTexCoord.y,
				width + trans.x, height + trans.y, 0.0f, endTexCoord.x, startTexCoord.y,
				-width + trans.x, height + trans.y, 0.0f,startTexCoord.x, startTexCoord.y,

				-width + trans.x, height + trans.y, 0.0f, startTexCoord.x, startTexCoord.y,
				-width + trans.x, -height + trans.y, 0.0f, startTexCoord.x,endTexCoord.y,
				width + trans.x, -height + trans.y, 0.0f, endTexCoord.x, endTexCoord.y,
			};
			return coords;
		}
	};

	SpriteSheet();
	SpriteSheet(Texture texture, int spriteWidth, int spriteHeight);
	SpriteSheet(Texture texture);
	~SpriteSheet();

	///<summary>
	///Returns texture coordinates of sprite (x1, x2, y1, y2).
	///</summary>
	glm::vec4 GetTexCoords(int index);

	///<summary>
	///Returns texture.
	///</summary>
	Texture GetTexture();

	///<summary>
	///Returns sprite.
	///</summary>
	Sprite GetSprite(int index);

	int sheetWidth, sheetHeight;
	int spriteWidth, spriteHeight;
	int rowSprites, colSprites;
private:
	Texture texture;
};