#pragma once

#include <vector>
#include <string>
#include <iostream>

#include <helpers/Importer.h>
#include <helpers/ByteCode.h>

class Cutscene
{
public:
	struct Elem {
		std::string name;
		std::string image;
		unsigned int order = 0;
		float x = 0, y = 0;
		float scale = 0;
	};

	struct Frame {
		std::string name;
		std::string bkg;
		std::vector<Elem> elems;
	};

	Cutscene();
	Cutscene(std::vector<char> data);
	~Cutscene();

	ByteCode onload;
	std::vector<Frame> frames;
};