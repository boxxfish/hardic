#include "stdafx.h"
#include "BCMachine.h"


BCMachine::BCMachine()
{
	this->callIndex = 0;
	this->codeIndex = 0;
	this->dataIndex = 0;
	this->waitTime = 0;
	this->lastTime = 0;
	this->disabled = false;
}

BCMachine::~BCMachine()
{
}

void BCMachine::UseByteCode(ByteCode b) {
	this->b = b;
	this->callIndex = 0;
	this->codeIndex = 0;
	this->dataIndex = 0;
	this->waitTime = 0;
	this->lastTime = 0;
	this->disabled = false;
}

void BCMachine::SetSceneEventQueue(EventQueue* eventQueue) {
	this->eventQueue = eventQueue;
}

void BCMachine::SetServices(Services* services) {
	this->services = services;
}

void BCMachine::Run() {
	if (this->NotWaiting(this->lastTime, this->services->time.GetTime()) && !this->disabled) {
		this->lastTime = this->services->time.GetTime();
		int instrCount = this->b.GetInstrCount();
		bool stop = false;
		while (this->codeIndex < instrCount && !stop) {
			char instr = this->b.GetInstruction(this->codeIndex);
			//Check instruction
			switch (instr) {
			//Empty instruction
			case ByteCode::Ops::NONE:
				break;
			//Literals
			case ByteCode::Ops::INT:
				this->PushInt(this->b.GetInt(this->codeIndex));
				break;
			case ByteCode::Ops::STRING:
				this->PushString(this->b.GetString(this->codeIndex));
				break;
			case ByteCode::Ops::CHAR:
				this->PushChar(this->b.GetChar(this->codeIndex));
				break;
			case ByteCode::Ops::BOOL:
				this->PushBool(this->b.GetBool(this->codeIndex));
				break;
			//Math
			case ByteCode::Ops::ADD:
				this->PushInt(this->PopInt() + this->PopInt());
				break;
			case ByteCode::Ops::SUB:
				this->PushInt(this->PopInt() - this->PopInt());
				break;
			case ByteCode::Ops::MULT:
				this->PushInt(this->PopInt() * this->PopInt());
				break;
			case ByteCode::Ops::DIV:
				this->PushInt(this->PopInt() / this->PopInt());
				break;
			case ByteCode::Ops::MOD:
				this->PushInt(this->PopInt() % this->PopInt());
				break;
			case ByteCode::Ops::POW:
				this->PushInt(std::pow(this->PopInt(), this->PopInt()));
				break;
			case ByteCode::Ops::ABS:
				this->PushInt(std::abs(this->PopInt()));
				break;
				//Stack
			case ByteCode::Ops::POP:
				this->PopChar();
				break;
			//Control Flow
			case ByteCode::Ops::IF:
				if (!this->PopBool())
					this->codeIndex++;
				break;
			case ByteCode::Ops::GOTO:
				this->codeIndex = this->PopInt() - 1;
				break;
			case ByteCode::Ops::AND:
				this->PushBool(this->PopBool() && this->PopBool());
				break;
			case ByteCode::Ops::OR:
				this->PushBool(this->PopBool() || this->PopBool());
				break;
			case ByteCode::Ops::IEQUAL:
				this->PushBool(this->PopInt() == this->PopInt());
				break;
			case ByteCode::Ops::BEQUAL:
				this->PushBool(this->PopBool() == this->PopBool());
				break;
			case ByteCode::Ops::CEQUAL:
				this->PushBool(this->PopChar() == this->PopChar());
				break;
			case ByteCode::Ops::SEQUAL:
				this->PushBool(this->PopString() == this->PopString());
				break;
			case ByteCode::Ops::MORE:
				this->PushBool(this->PopInt() > this->PopInt());
				break;
			case ByteCode::Ops::LESS:
				this->PushBool(this->PopInt() < this->PopInt());
				break;
			case ByteCode::Ops::WAIT:
				this->waitTime = this->PopInt() / 10.0f;
				stop = true;
				break;
			case ByteCode::Ops::END:
				this->codeIndex = instrCount;
				break;
			case ByteCode::Ops::CALL:
				this->PushCall(this->codeIndex);
				this->codeIndex = this->PopInt() - 1;
				break;
			case ByteCode::Ops::RETURN:
				this->codeIndex = this->PopCall();
				break;
			//Game Ops
			case ByteCode::Ops::FOCUS:
				this->PopString();
				break;
			case ByteCode::Ops::TALK:
				this->eventQueue->Broadcast(new DialogueEvent(this->PopString(), this->PopString()));
				stop = true;
				this->disabled = true;
				break;
			case ByteCode::Ops::TOROOM:
				this->services->sysEventQueue.Broadcast(new ToRoomEvent(this->PopString()));
				break;
			case ByteCode::Ops::TOWORLD:
				this->services->sysEventQueue.Broadcast(new SceneChangeEvent(this->PopString()));
				break;
			case ByteCode::Ops::TOCS:
				this->services->sysEventQueue.Broadcast(new ToCutsceneEvent(this->PopString()));
				break;
			case ByteCode::Ops::SAVEVAL:
				this->PopString();
				this->PopString();
				//this->services->saveManager.saveData.SaveString(this->PopString(), this->PopString());
				break;
			case ByteCode::Ops::LOADVAL:
				this->PopString();
				//this->PushString(this->services->saveManager.saveData.RecallString(this->PopString()));
				break;
			case ByteCode::Ops::NORM:
				this->eventQueue->Broadcast(new NormalEvent());
				break;
			case ByteCode::Ops::SETFRAME:
				this->eventQueue->Broadcast(new FrameSetEvent(this->PopString()));
				break;
			case ByteCode::Ops::SETELEMPOS:
				this->PopString();
				this->PopInt();
				this->PopInt();
				break;
			case ByteCode::Ops::MOVEELEM:
				this->eventQueue->Broadcast(new MoveElemEvent(	this->PopString(),
																(this->services->cam2d.GetAspect() * this->services->cam2d.GetSize()) - (this->services->cam2d.GetAspect() * this->services->cam2d.GetSize() * (this->PopInt() / 100.0f) * 2),
																(this->services->cam2d.GetSize()) - (this->services->cam2d.GetSize() * (this->PopInt() / 100.0f) * 2),
																(this->services->cam2d.GetAspect() * this->services->cam2d.GetSize()) - (this->services->cam2d.GetAspect() * this->services->cam2d.GetSize() * (this->PopInt() / 100.0f) * 2),
																(this->services->cam2d.GetSize()) - (this->services->cam2d.GetSize() * (this->PopInt() / 100.0f) * 2),
																this->PopInt() / 10.0f,
																false));
				break;
			case ByteCode::Ops::MOVEELEMSM:
				this->eventQueue->Broadcast(new MoveElemEvent(	this->PopString(),
																(this->services->cam2d.GetAspect() * this->services->cam2d.GetSize()) - (this->services->cam2d.GetAspect() * this->services->cam2d.GetSize() * (this->PopInt() / 100.0f) * 2),
																(this->services->cam2d.GetSize()) - (this->services->cam2d.GetSize() * (this->PopInt() / 100.0f) * 2),
																(this->services->cam2d.GetAspect() * this->services->cam2d.GetSize()) - (this->services->cam2d.GetAspect() * this->services->cam2d.GetSize() * (this->PopInt() / 100.0f) * 2),
																(this->services->cam2d.GetSize()) - (this->services->cam2d.GetSize() * (this->PopInt() / 100.0f) * 2),
																this->PopInt() / 10.0f,
																true));
				break;
			}
			//Next instruction
			this->codeIndex++;
		}
	}
}

void BCMachine::PushInt(int val) {
	this->dataStack[this->dataIndex++] = val;
}

void BCMachine::PushString(std::string val) {
	//Chars, then char count
	std::memcpy(this->dataStack + this->dataIndex, val.c_str(), val.size());
	this->dataIndex += val.size();
	this->dataStack[this->dataIndex++] = val.size();
}

void BCMachine::PushChar(char val) {
	this->dataStack[this->dataIndex++] = val;
}

void BCMachine::PushBool(bool val) {
	this->dataStack[this->dataIndex++] = val ? 0x01 : 0x00;
}

void BCMachine::PushCall(int lineNum) {
	this->callStack[this->callIndex++] = lineNum;
}

short int BCMachine::PopInt() {
	return this->dataStack[--this->dataIndex];
}

std::string BCMachine::PopString() {
	//Get char count
	int charCount =	this->dataStack[--this->dataIndex];
	//Copy chars from data stack
	this->dataIndex -= charCount;
	char* tempStr = new char[charCount + 1];
	std::memcpy(tempStr, this->dataStack + this->dataIndex, charCount);
	//Add string terminator to end of array
	tempStr[charCount] = '\0';
	//Create return string
	std::string val = tempStr;
	//Delete allocated array
	delete[] tempStr;
	return val;
}

char BCMachine::PopChar() {
	return this->dataStack[--this->dataIndex];
}

bool BCMachine::PopBool() {
	return this->dataStack[--this->dataIndex] == 0x01;
}

int BCMachine::PopCall() {
	return this->callStack[--this->callIndex];
}

bool BCMachine::NotWaiting(float lastTime, float currTime) {
	return currTime - lastTime >= this->waitTime;
}

void BCMachine::Enable() {
	this->disabled = false;
}
