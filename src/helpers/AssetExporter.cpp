#include "stdafx.h"
#include "AssetExporter.h"

AssetExporter::AssetExporter()
{
	this->innerIndex = 0;
}

AssetExporter::~AssetExporter()
{
}

void AssetExporter::WriteLine(std::string line) {
	this->data += this->GenTabs(this->innerIndex) + line + "\n";
}

void AssetExporter::WriteKeyValue(std::string key, std::string value) {
	this->data += this->GenTabs(this->innerIndex) + key + ": " + value + "\n";
}

void AssetExporter::WriteKeyValue(std::string key, std::vector<std::string> values, int lineWidth) {
	this->data += this->GenTabs(this->innerIndex) + key + ": {\n";
	this->innerIndex++;
	this->data += this->GenTabs(this->innerIndex);
	for (int i = 0; i < values.size(); i++) {
		this->data += values[i];
		if (i < values.size() - 1) {
			this->data += ", ";
			if ((i + 1) % lineWidth == 0)
				this->data += "\n" + this->GenTabs(this->innerIndex);
		}
	}
	this->innerIndex--;
	this->data += "\n" + this->GenTabs(this->innerIndex) + "}";
}

void AssetExporter::WriteArraySeparator() {
	this->data += "," + this->GenTabs(this->innerIndex) + "\n";
}

void AssetExporter::WriteObjectStart(std::string name) {
	this->data += this->GenTabs(this->innerIndex) + name + ": {\n";
	this->innerIndex++;
}

void AssetExporter::WriteObjectEnd() {
	this->innerIndex--;
	this->data += "\n" + this->GenTabs(this->innerIndex) + "}\n";
}

void AssetExporter::WriteFile(std::string address) {
	std::ofstream file(address);
	file.write(this->data.c_str(), this->data.size());
}

std::string AssetExporter::GenTabs(int innerIndex) {
	std::string tab;
	for (int i = 0; i < innerIndex; i++)
		tab += "\t";
	return tab;
}
