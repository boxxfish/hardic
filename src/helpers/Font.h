#pragma once

#include <ft2build.h>
#include FT_FREETYPE_H

#include <iostream>
#include <vector>
#include <string>

#include "Texture.h"
#include "BoundingBox.h"

class Font
{
public:
	Font();
	Font(std::vector<char> data, int size, bool antiAliased = true);
	~Font();

	Texture GetTexture();

	BoundingBox GetCharBounds(char character);
	glm::vec2 GetCharAdvance(char character);
	glm::vec2 GetCharBaseLine(char character);
private:
	Texture tex;
	std::map<char, BoundingBox> bounds;
	std::map<char, glm::vec2> advances;
	std::map<char, glm::vec2> baseLines;
};

