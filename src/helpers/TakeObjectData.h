#pragma once

#include <string>

#include <helpers/SpriteSheet.h>

class TakeObjectData
{
public:
	TakeObjectData();
	TakeObjectData(std::string name, std::string desc, SpriteSheet::Sprite sprite);
	~TakeObjectData();

	std::string name;
	std::string desc;
	SpriteSheet::Sprite sprite;
};