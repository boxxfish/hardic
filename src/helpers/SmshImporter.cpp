#include "stdafx.h"
#include "SmshImporter.h"

Model SmshImporter::Import(std::vector<char> data) {
	Model model;
	
	Importer importer(data);

	//Header
	if ((!(importer.ReadChar() == 'S' && importer.ReadChar() == 'M' && importer.ReadChar() == 'S' && importer.ReadChar() == 'H'))) {
		std::cout << "Error: File is not a valid SMSH file.\n";
		return model;
	}

	//Meshes
	int meshCount = importer.ReadUint8();
	for (int i = 0; i < meshCount; i++) {
		Model::ModelMesh currMesh;
		currMesh.tex = importer.ReadString();
		int coordCount = importer.ReadUint16();
		//Vertices
		for (int j = 0; j < coordCount; j++) {
			currMesh.verts.push_back(importer.ReadFloat());
			currMesh.verts.push_back(importer.ReadFloat());
			currMesh.verts.push_back(importer.ReadFloat());
			//TexCoords
			currMesh.texCoords.push_back(importer.ReadFloat());
			currMesh.texCoords.push_back(importer.ReadFloat());
			//Normals
			currMesh.normals.push_back(importer.ReadFloat());
			currMesh.normals.push_back(importer.ReadFloat());
			currMesh.normals.push_back(importer.ReadFloat());
		}
		model.meshes.push_back(currMesh);
	}
	return model;
}
