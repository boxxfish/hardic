#include "stdafx.h"
#include "LevelAssetExporter.h"

void LevelAssetExporter::Export(LevelMap lvlMap, std::string path) {
	AssetExporter exporter;

	//Name
	exporter.WriteLine("MAP");

	//Tileset
	exporter.WriteKeyValue("TILESET", lvlMap.tilesetName);

	//Dimensions
	exporter.WriteKeyValue("WIDTH", std::to_string(lvlMap.width));
	exporter.WriteKeyValue("HEIGHT", std::to_string(lvlMap.height));
	exporter.WriteKeyValue("DEPTH", std::to_string(lvlMap.depth));

	//Tile layers
	exporter.WriteObjectStart("TILES");
	//First tile layer
	std::vector<std::string> tiles;
	for (int y = 0; y < lvlMap.height; y++) {
		for (int z = 0; z < lvlMap.depth; z++) {
			for (int x = 0; x < lvlMap.width; x++) {
				tiles.push_back(std::to_string(lvlMap.tileLayers[0][y][z][x]));
			}
		}
	}
	exporter.WriteKeyValue(std::to_string(0), tiles, lvlMap.width);
	for (int i = 1; i < lvlMap.tileLayers.size(); i++) {
		exporter.WriteLine(",");
		std::vector<std::string> tiles;
		for (int y = 0; y < lvlMap.height; y++) {
			for (int z = 0; z < lvlMap.depth; z++) {
				for (int x = 0; x < lvlMap.width; x++) {
					tiles.push_back(std::to_string(lvlMap.tileLayers[i][y][z][x]));
				}
			}
		}
		exporter.WriteKeyValue(std::to_string(i), tiles, lvlMap.width);
	}
	exporter.WriteObjectEnd();

	//Object layers
	exporter.WriteObjectStart("OBJECTS");
	//First object layer
	std::vector<std::string> objs;
	for (int z = 0; z < lvlMap.depth; z++) {
		for (int x = 0; x < lvlMap.width; x++) {
			objs.push_back(std::to_string(lvlMap.objectLayers[0][z][x]));
		}
	}
	exporter.WriteKeyValue(std::to_string(0), objs, lvlMap.width);
	for (int i = 1; i < lvlMap.objectLayers.size(); i++) {
		exporter.WriteLine(",");
		std::vector<std::string> objs;
		for (int z = 0; z < lvlMap.depth; z++) {
			for (int x = 0; x < lvlMap.width; x++) {
				objs.push_back(std::to_string(lvlMap.objectLayers[i][z][x]));
			}
		}
		exporter.WriteKeyValue(std::to_string(i), objs, lvlMap.width);
	}
	exporter.WriteObjectEnd();

	//Move layer
	std::vector<std::string> move;
	for (int z = 0; z < lvlMap.depth; z++) {
		for (int x = 0; x < lvlMap.width; x++) {
			move.push_back(std::to_string(lvlMap.moveLayer[z][x]));
		}
	}
	exporter.WriteKeyValue("MOVE", move, lvlMap.width);

	exporter.WriteFile(path);
}
