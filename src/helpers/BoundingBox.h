#pragma once

#include <glm/glm.hpp>

class BoundingBox
{
public:
	BoundingBox();
	BoundingBox(glm::vec2 position, glm::vec2 size);
	~BoundingBox();

	///<summary>
	///Checks if a point is within the box's bounds.
	///</summary>
	bool ContainsPoint(glm::vec2 point);

	glm::vec2 position;
	glm::vec2 size;
};

