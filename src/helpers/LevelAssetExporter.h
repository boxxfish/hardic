#pragma once

#include <string>

#include <LevelMap.h>
#include <helpers/AssetExporter.h>

class LevelAssetExporter
{
public:
	static void Export(LevelMap lvlMap, std::string path);
};