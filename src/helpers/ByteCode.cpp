#include "stdafx.h"
#include "ByteCode.h"

ByteCode::ByteCode()
{
	this->dataSize = 0;
	this->instrSize = 0;
}


ByteCode::ByteCode(std::string src) {
	this->dataSize = 0;
	this->instrSize = 0;
	//Split string by newline
	std::vector<std::string> lines = Utilities::Split(src, '\n');
	//Iterate through lines
	this->instrSize = lines.size();
	for (int i = 0; i < lines.size(); i++) {
		std::string line = lines[i];
		//Set current instruction index to current data offset
		this->instr[i] = this->dataSize;
		//Null instruction for comments
		if (line[0] == '#') {
			this->data[this->dataSize] = this->NONE;
		}
		//Literals
		if (line.substr(0, 3) == "INT") {
			this->data[this->dataSize] = Ops::INT;
			this->dataSize++;
			this->data[this->dataSize] = std::stoi(line.substr(3, line.size() - 3));
		}
		if (line.substr(0, 6) == "STRING") {
			//Strings are saved the char count, followed by a series of chars
			this->data[this->dataSize] = Ops::STRING;
			this->dataSize++;
			std::string val = line.substr(7, line.size() - 7);
			this->data[this->dataSize] = val.size();
			this->dataSize++;
			std::memcpy(this->data + this->dataSize, val.c_str(), val.size());
			this->dataSize += val.size() - 1;
		}
		if (line.substr(0, 4) == "CHAR") {
			this->data[this->dataSize] = Ops::CHAR;
			this->dataSize++;
			this->data[this->dataSize] = line.substr(4, line.size() - 4)[0];
		}
		if (line.substr(0, 4) == "BOOL") {
			//0x00 if false, 0x01 if true
			this->data[this->dataSize] = Ops::BOOL;
			this->dataSize++;
			this->data[this->dataSize] = line.substr(4, line.size() - 4) == "false" ? 0x00 : 0x01;
		}
		//Math
		if (line == "ADD")
			this->data[this->dataSize] = Ops::ADD;
		if (line == "SUB")
			this->data[this->dataSize] = Ops::SUB;
		if (line == "MULT")
			this->data[this->dataSize] = Ops::MULT;
		if (line == "DIV")
			this->data[this->dataSize] = Ops::DIV;
		if (line == "MOD")
			this->data[this->dataSize] = Ops::MOD;
		if (line == "POW")
			this->data[this->dataSize] = Ops::POW;
		if (line == "ABS")
			this->data[this->dataSize] = Ops::ABS;
		//Stack
		if (line == "POP")
			this->data[this->dataSize] = Ops::POP;
		//Control Flow
		if (line == "IF")
			this->data[this->dataSize] = Ops::IF;
		if (line == "GOTO")
			this->data[this->dataSize] = Ops::GOTO;
		if (line == "AND")
			this->data[this->dataSize] = Ops::AND;
		if (line == "OR")
			this->data[this->dataSize] = Ops::OR;
		if (line == "IEQUAL")
			this->data[this->dataSize] = Ops::IEQUAL;
		if (line == "BEQUAL")
			this->data[this->dataSize] = Ops::BEQUAL;
		if (line == "CEQUAL")
			this->data[this->dataSize] = Ops::CEQUAL;
		if (line == "SEQUAL")
			this->data[this->dataSize] = Ops::SEQUAL;
		if (line == "MORE")
			this->data[this->dataSize] = Ops::MORE;
		if (line == "LESS")
			this->data[this->dataSize] = Ops::LESS;
		if (line == "WAIT")
			this->data[this->dataSize] = Ops::WAIT;
		if (line == "END")
			this->data[this->dataSize] = Ops::END;
		if (line == "CALL")
			this->data[this->dataSize] = Ops::CALL;
		if (line == "RETURN")
			this->data[this->dataSize] = Ops::RETURN;
		//Game Ops
		if (line == "FOCUS")
			this->data[this->dataSize] = Ops::FOCUS;
		if (line == "TALK")
			this->data[this->dataSize] = Ops::TALK;
		if (line == "TOROOM")
			this->data[this->dataSize] = Ops::TOROOM;
		if (line == "TOWORLD")
			this->data[this->dataSize] = Ops::TOWORLD;
		if (line == "TOCS")
			this->data[this->dataSize] = Ops::TOCS;
		if (line == "SAVEVAL")
			this->data[this->dataSize] = Ops::SAVEVAL;
		if (line == "LOADVAL")
			this->data[this->dataSize] = Ops::LOADVAL;
		if (line == "NORM")
			this->data[this->dataSize] = Ops::NORM;
		if (line == "SETFRAME")
			this->data[this->dataSize] = Ops::SETFRAME;
		if (line == "SETELEMPOS")
			this->data[this->dataSize] = Ops::SETELEMPOS;
		if (line == "MOVEELEM")
			this->data[this->dataSize] = Ops::MOVEELEM;
		if (line == "MOVEELEMSM")
			this->data[this->dataSize] = Ops::MOVEELEMSM;
		//Increase the data size by 1
		this->dataSize++;
	}
}

ByteCode::ByteCode(std::vector<char> data, std::vector<char> instr) {
	//Copy data vector into data array
	this->dataSize = data.size();
	std::memcpy(this->data, data.data(), this->dataSize);
	//Copy instr vector into instr array
	this->instrSize = instr.size();
	std::memcpy(this->instr, instr.data(), this->instrSize);
}

ByteCode::~ByteCode()
{
}

char ByteCode::GetInstruction(unsigned int line) {
	return this->data[this->instr[line]];
}

int ByteCode::GetInt(unsigned int line) {
	return this->data[this->instr[line] + 1];
}

std::string ByteCode::GetString(unsigned int line) {
	int charCount = this->data[this->instr[line] + 1];
	std::string val;
	for (int i = 0; i < charCount; i++)
		val += this->data[this->instr[line] + 2 + i];
	return val;
}

char ByteCode::GetChar(unsigned int line) {
	return this->data[this->instr[line] + 1];
}

bool ByteCode::GetBool(unsigned int line) {
	return this->data[this->instr[line] + 1] == 0x00 ? false : true;
}

unsigned int ByteCode::GetInstrCount() {
	return this->instrSize;
}

std::vector<char> ByteCode::GetData() {
	return std::vector<char>(this->data, this->data + this->dataSize);
}

std::vector<char> ByteCode::GetInstr() {
	return std::vector<char>(this->instr, this->instr + this->instrSize);
}
