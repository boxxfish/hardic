#include "stdafx.h"
#include "Font.h"


Font::Font() {
	
}

Font::Font(std::vector<char> data, int size, bool antiAliased) {
	//Init library
	FT_Library lib;
	FT_Error err = FT_Init_FreeType(&lib);
	if (err) {
		std::cout << "Error: Could not initialize freetype.\n";
		return;
	}
	//Create face
	FT_Face face;
	err = FT_New_Memory_Face(lib, (unsigned char*)&data[0], data.size(), 0, &face);
	if (err) {
		std::cout << "Error: Could not read font file.\n";
	}
	//Set character size
	err = FT_Set_Char_Size(face, 0, size * 64, 72, 72);
	if (err) {
		std::cout << "Error: Could not set character size.\n";
	}
	//Create character map
	std::vector<char> chars = {
		'A',
		'B',
		'C',
		'D',
		'E',
		'F',
		'G',
		'H',
		'I',
		'J',
		'K',
		'L',
		'M',
		'N',
		'O',
		'P',
		'Q',
		'R',
		'S',
		'T',
		'U',
		'V',
		'W',
		'X',
		'Y',
		'Z',
		'a',
		'b',
		'c',
		'd',
		'e',
		'f',
		'g',
		'h',
		'i',
		'j',
		'k',
		'l',
		'm',
		'n',
		'o',
		'p',
		'q',
		'r',
		's',
		't',
		'u',
		'v',
		'w',
		'x',
		'y',
		'z',
		'0',
		'1',
		'2',
		'3',
		'4',
		'5',
		'6',
		'7',
		'8',
		'9',
		'!',
		'@',
		'#',
		'$',
		'%',
		'^',
		'&',
		'*',
		'(',
		')',
		'-',
		'_',
		'+',
		'=',
		'{',
		'}',
		'[',
		']',
		';',
		':',
		'\'',
		'\"',
		'\\',
		'<',
		',',
		'>',
		'.',
		'?',
		'/',
		'~',
		'`',
		' '
	};
	std::vector<std::vector<unsigned char>> pixels;
	int width = 0;
	int height = 0;
	for (int i = 0; i < chars.size(); i++) {
		//Load glyph
		err = FT_Load_Char(face, chars[i], FT_LOAD_RENDER);
		if (err) {
			std::cout << "Error: Could not create glyph bitmap.\n";
		}
		FT_GlyphSlot slot = face->glyph;
		FT_Bitmap bitmap = slot->bitmap;
		//Set character bounds
		BoundingBox charBounds;
		charBounds.size = glm::vec2(bitmap.width, bitmap.rows);
		charBounds.position = glm::vec2(width, 0);
		this->bounds[chars[i]] = charBounds;
		//Set character advances
		this->advances[chars[i]] = glm::vec2(slot->advance.x / 64, slot->advance.y / 64);
		//Set character baselines
		this->baseLines[chars[i]] = glm::vec2(slot->bitmap_left, slot->bitmap_top);
		//Write glyph bitmap into character map
		if (bitmap.rows > height) {
			//Push back more rows
			for (int j = 0; j < bitmap.rows - height; j++)
				pixels.push_back(std::vector<unsigned char>(width * 4, 0));
			height = bitmap.rows;
		}
		width += bitmap.width;
		for (int y = 0; y < bitmap.rows; y++) {
			for (int x = 0; x < bitmap.width; x++) {
				//Fill all 4 channels
				pixels[y].push_back(bitmap.buffer[y * bitmap.width + (bitmap.width - x - 1)]);
				pixels[y].push_back(bitmap.buffer[y * bitmap.width + (bitmap.width - x - 1)]);
				pixels[y].push_back(bitmap.buffer[y * bitmap.width + (bitmap.width - x - 1)]);
				pixels[y].push_back(bitmap.buffer[y * bitmap.width + (bitmap.width - x - 1)]);
			}
		}
		//Insert empty elements in unused rows
		for (int y = bitmap.rows; y < height; y++) {
			for (int x = 0; x < bitmap.width * 4; x++)
				pixels[y].push_back(0);
		}
	}
	//Turn pixels into 1 dimensional vector
	std::vector<unsigned char> texData;
	for (int y = 0; y < pixels.size(); y++) {
		for (int x = 0; x < pixels[0].size(); x++) {
			texData.push_back(pixels[y][x]);
		}
	}
	//Create texture
	Texture::CreateTexture(std::string(face->family_name) + std::to_string(size), width, height, texData, !antiAliased);
	this->tex = Texture::FindTexture(std::string(face->family_name) + std::to_string(size));
	//Deallocate face and library
	FT_Done_Face(face);
	FT_Done_FreeType(lib);
}

Font::~Font()
{
}

Texture Font::GetTexture() {
	return tex;
}

BoundingBox Font::GetCharBounds(char character) {
	return this->bounds[character];
}

glm::vec2 Font::GetCharAdvance(char character) {
	return advances[character];
}

glm::vec2 Font::GetCharBaseLine(char character) {
	return this->baseLines[character];
}
