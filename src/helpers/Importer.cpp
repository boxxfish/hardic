#include "stdafx.h"
#include "Importer.h"


Importer::Importer() {
	this->index = 0;
}

Importer::Importer(std::vector<char> data) {
	this->data = data;
	this->index = 0;
}


Importer::~Importer()
{
}

char Importer::ReadChar() {
	return this->data[this->index++];
}

unsigned int Importer::ReadUint8() {
	return this->ReadChar();
}

int Importer::ReadInt8() {
	return this->ReadChar();
}

unsigned int Importer::ReadUint16() {
	unsigned int value = 0;
	unsigned char byte1 = this->ReadChar();
	unsigned char byte2 = this->ReadChar();
	value = byte1 << 8 | byte2;
	return value;
}

int Importer::ReadInt16() {
	int value = 0;
	char byte1 = this->ReadChar();
	char byte2 = this->ReadChar();
	value = byte1 << 8 | byte2;
	return value;
}

float Importer::ReadFloat() {
	char charPtr[4];
	charPtr[0] = this->ReadChar();
	charPtr[1] = this->ReadChar();
	charPtr[2] = this->ReadChar();
	charPtr[3] = this->ReadChar();
	float* val = 0;
	return *reinterpret_cast<float*>(charPtr);
}

std::string Importer::ReadString() {
	int numChars = this->ReadUint8();
	std::string val;
	for (int i = 0; i < numChars; i++)
		val += this->ReadChar();
	return val;
}

bool Importer::ReadBool() {
	return this->ReadChar();
}
