#pragma once

#include <vector>
#include <iostream>

#include <engine/Renderer.h>
#include "Importer.h"
#include "Model.h"

class SmshImporter
{
public:
	static Model Import(std::vector<char> data);
};