#pragma once

#include <string>
#include <vector>
#include <iostream>
#include <fstream>

class AssetExporter
{
public:
	AssetExporter();
	~AssetExporter();

	void WriteLine(std::string line);
	void WriteKeyValue(std::string key, std::string value);
	void WriteKeyValue(std::string key, std::vector<std::string> values, int lineWidth = 1);
	
	void WriteArraySeparator();

	void WriteObjectStart(std::string name);
	void WriteObjectEnd();

	void WriteFile(std::string address);
private:
	std::string GenTabs(int innerIndex);

	std::string data;
	int innerIndex;
};