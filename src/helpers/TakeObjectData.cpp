#include "stdafx.h"
#include "TakeObjectData.h"

TakeObjectData::TakeObjectData()
{
}

TakeObjectData::TakeObjectData(std::string name, std::string desc, SpriteSheet::Sprite sprite) 
	: name(name), desc(desc), sprite(sprite)
{
}

TakeObjectData::~TakeObjectData()
{
}
