#pragma once

#include <vector>
#include <glm/glm.hpp>

template <typename T>
class ObjectLayer3D
{
public:
	ObjectLayer3D() {
		this->width = 0;
		this->height = 0;
		this->depth = 0;
	}
	ObjectLayer3D(unsigned int width, unsigned int height, unsigned int depth, T defaultValue = NULL) {
		this->width = width;
		this->height = height;
		this->depth = depth;
		this->defaultValue = defaultValue;
		this->data = std::vector<std::vector<std::vector<T>>>(this->height);
		for (int y = 0; y < this->height; y++)
		this->data[y] = std::vector<T>(this->width, defaultValue);
	}
	~ObjectLayer3D() {}
	
	///<summary>
	///Sets value at specified location.
	///</summary>
	void SetValue(unsigned int x, unsigned int y, T value) {
		if (x >= this->width || y >= this->height)
			return;
		this->data[y][x] = value;
	}

	///<summary>
	///Returns value at specified location.
	///</summary>
	T GetValue(unsigned int x, unsigned int y) {
		if (x >= this->width || y >= this->height)
			return this->defaultValue;
		return this->data[y][x];
	}

	///<summary>
	///Returns location of first instance of value if it exists.
	///</summary>
	glm::ivec2 GetFirstInstance(T value) {
		for (int y = 0; y < this->height; y++) {
			for (int x = 0; x < this->width; x++) {
				if (this->data[y][x] == value)
					return glm::ivec2(x, y);
			}
		}
		return glm::ivec2();
	}
private:
	std::vector<std::vector<std::vector<T>>> data;
	unsigned int width, height, depth;
	T defaultValue;
};