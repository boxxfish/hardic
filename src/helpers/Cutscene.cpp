#include "stdafx.h"
#include "Cutscene.h"

Cutscene::Cutscene()
{
}

Cutscene::Cutscene(std::vector<char> data) {
	Importer importer(data);

	//Header
	if (!(importer.ReadChar() == 'C' && importer.ReadChar() == 'T' && importer.ReadChar() == 'S' && importer.ReadChar() == 'C')) {
		std::cout << "Error: File is not a valid cutscene file.\n";
		return;
	}

	//Onload
	std::vector<char> dataVec;
	int dataSize = importer.ReadUint16();
	for (int i = 0; i < dataSize; i++)
		dataVec.push_back(importer.ReadChar());
	std::vector<char> instrVec;
	int instrSize = importer.ReadUint8();
	for (int i = 0; i < instrSize; i++)
		instrVec.push_back(importer.ReadChar());
	this->onload = ByteCode(dataVec, instrVec);

	//Frames
	int frameCount = importer.ReadUint8();
	for (int i = 0; i < frameCount; i++) {
		Frame frame;
		frame.name = importer.ReadString();
		frame.bkg = importer.ReadString();
		//Elements
		int elemCount = importer.ReadUint8();
		for (int j = 0; j < elemCount; j++) {
			Elem elem;
			elem.name = importer.ReadString();
			elem.image = importer.ReadString();
			elem.order = importer.ReadUint8();
			elem.x = importer.ReadFloat();
			elem.y = importer.ReadFloat();
			elem.scale = importer.ReadFloat();
			frame.elems.push_back(elem);
		}
		this->frames.push_back(frame);
	}
}

Cutscene::~Cutscene()
{
}
