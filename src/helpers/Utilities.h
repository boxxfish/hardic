#pragma once

#include <glm/glm.hpp>

#include <string>
#include <vector>
#include <ctype.h>

static class Utilities
{
public:
	///<summary>
	///Checks if two floats are approximately equal to each other.
	///</summary>
	static bool Approximately(float a, float b, float fudge = 0.1f);

	///<summary>
	///Checks if two vec2s are approximately equal to each other.
	///</summary>
	static bool Approximately(glm::vec2 a, glm::vec2 b, float fudge = 0.1f);

	///<summary>
	///Splits source string.
	///</summary>
	static std::vector<std::string> Split(std::string string, char delimiter = ' ');

	///<summary>
	///Clamps a value between a min and max.
	///</summary>
	static float Clamp(float min, float max, float value);

	///<summary>
	///Converts string to uppercase.
	///</summary>
	static std::string ToUpper(std::string string);

	///<summary>
	///Converts string to lowercase.
	///</summary>
	static std::string ToLower(std::string string);
};