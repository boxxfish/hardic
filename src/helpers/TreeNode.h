#pragma once

#include <iostream>
#include <vector>
#include <memory>
//TODO: Comment what functions do
template<typename T>
class TreeNode
{
public:
	TreeNode<T>() {};
	~TreeNode() {};

	TreeNode* GetParent() {
		return this->parent;
	}

	std::shared_ptr<TreeNode> CreateChild(std::string name = "") {
		std::shared_ptr<TreeNode> child = std::make_shared<TreeNode>();
		child->name = name;
		child->parent = this;
		this->children.push_back(child);
		return child;
	}

	void AddChild(std::shared_ptr<TreeNode> child) {
		child->parent = this;
		this->children.push_back(child);
	}

	std::shared_ptr<TreeNode> FindChild(std::string name) {
		for (int i = 0; i < this->children.size(); i++) {
			if (this->children[i]->name == name)
				return this->children[i];
		}
		std::cout << "Error: Could not find node \"" << name.c_str() << "\" in file tree.\n";
		return nullptr;
	}

	std::shared_ptr<TreeNode> GetChild(int index) {
		if (this->children.size() <= index) {
			std::cout << "Error: 'GetChild' index is larger than number of children.\n";
			return nullptr;
		}
		return this->children[index];
	}

	void TreeNode::RemoveChild(int index) {
		if (this->children.size() <= index) {
			std::cout << "Error: 'RemoveChild' index is larger than number of children.\n";
			return;
		}
		this->children.erase(this->children.begin() + index);
	}

	void RemoveChild(std::string name) {
		for (int i = 0; i < this->children.size(); i++) {
			if (this->children[i]->name == name)
				this->RemoveChild(i);
		}
		std::cout << "Error: Could not find node \"" << name.c_str() << "\" in file tree.\n";
		return;
	}

	unsigned int GetChildCount() {
		return this->children.size();
	}

	void TreeNode::Display() {
		DisplayNode(*this, 0);
	}

	std::string name;
	T data;
private:
	void DisplayNode(TreeNode node, int layer) {
		//Display current node
		std::string indent;
		for (int i = 0; i < layer; i++)
			indent += "--";
		std::cout << indent.c_str() << node.name.c_str() << "\n";
		//Repeat for child nodes
		for (int i = 0; i < node.children.size(); i++)
			this->DisplayNode(*node.children[i], layer + 1);
	}

	TreeNode* parent;
	std::vector<std::shared_ptr<TreeNode>> children;
};

