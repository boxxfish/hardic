#include "stdafx.h"
#include "Utilities.h"

bool Utilities::Approximately(float a, float b, float fudge) {
	return std::abs(a - b) < fudge;
}

bool Utilities::Approximately(glm::vec2 a, glm::vec2 b, float fudge) {
	return Utilities::Approximately(a.x, b.x, fudge) && Utilities::Approximately(a.y, b.y, fudge);
}

std::vector<std::string> Utilities::Split(std::string string, char delimiter) {
	std::vector<std::string> results;
	std::string token = "";
	for (int i = 0; i < string.size(); i++) {
		if (string[i] == delimiter) {
			results.push_back(token);
			token = "";
		}
		else
			token += string[i];
	}
	results.push_back(token);
	return results;
}

float Utilities::Clamp(float min, float max, float value) {
	if (value < min)
		return min;
	if (value > max)
		return max;
	return value;
}

std::string Utilities::ToUpper(std::string string) {
	std::string val;
	for (int i = 0; i < string.size(); i++)
		val += toupper(string[i]);
	return val;
}

std::string Utilities::ToLower(std::string string) {
	std::string val;
	for (int i = 0; i < string.size(); i++)
		val += tolower(string[i]);
	return val;
}
