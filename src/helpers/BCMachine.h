#pragma once

#include <helpers/ByteCode.h>
#include <engine/Services.h>
#include <eventsystem/NormalEvent.h>
#include <eventsystem/DialogueEvent.h>
#include <eventsystem/FrameSetEvent.h>
#include <eventsystem/MoveElemEvent.h>
#include <eventsystem/SceneChangeEvent.h>
#include <eventsystem/ToRoomEvent.h>
#include <eventsystem/ToCutsceneEvent.h>

class BCMachine
{
public:
	BCMachine();
	~BCMachine();

	void UseByteCode(ByteCode b);

	void SetSceneEventQueue(EventQueue* eventQueue);

	void SetServices(Services* services);

	void Run();

	bool NotWaiting(float lastTime, float currTime);

	void Enable();
private:
	void PushInt(int val);
	void PushString(std::string val);
	void PushChar(char val);
	void PushBool(bool val);
	void PushCall(int lineNum);

	short int PopInt();
	std::string PopString();
	char PopChar();
	bool PopBool();
	int PopCall();

	const static unsigned int MAX_DATA_STACK_SIZE = 64;
	const static unsigned int MAX_CALL_STACK_SIZE = 4;

	ByteCode b;
	unsigned char dataStack[MAX_DATA_STACK_SIZE];
	unsigned char callStack[MAX_CALL_STACK_SIZE];
	unsigned int dataIndex;
	unsigned int callIndex;
	unsigned int codeIndex;
	
	EventQueue* eventQueue;
	Services* services;
	float waitTime;
	float lastTime;
	bool disabled;
};