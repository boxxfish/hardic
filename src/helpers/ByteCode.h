#pragma once

#include <string>
#include <vector>

#include "Utilities.h"

class ByteCode
{
public:
	enum Ops {
		NONE =		0x00,
		//Literals
		INT =		0x01,
		STRING =	0x02,
		CHAR =		0x03,
		BOOL =		0x04,
		//Math
		ADD =		0x05,
		SUB =		0x06,
		MULT =		0x07,
		DIV =		0x08,
		MOD =		0x09,
		POW =		0x0A,
		ABS =		0x0B,
		//Stack
		POP =		0x0C,
		//Control Flow
		IF =		0x0D,
		GOTO =		0x0E,
		AND =		0x0F,
		OR =		0x10,
		IEQUAL =	0x11,
		BEQUAL =	0x12,
		CEQUAL =	0x13,
		SEQUAL =	0x14,
		MORE =		0x15,
		LESS =		0x16,
		WAIT =		0x17,
		END =		0x18,
		CALL =		0x19,
		RETURN =	0x1A,
		//Game Ops
		FOCUS =		0x1B,
		TALK =		0x1C,
		TOROOM =	0x1D,
		TOWORLD =	0x1E,
		TOCS =		0x1F,
		SAVEVAL =	0x20,
		LOADVAL =	0x21,
		NORM =		0x22,
		SETFRAME =	0x23,
		SETELEMPOS =0x24,
		MOVEELEM =	0x25,
		MOVEELEMSM =0x26,
	};

	ByteCode();
	ByteCode(std::string src);
	ByteCode(std::vector<char> data, std::vector<char> instr);
	~ByteCode();

	char GetInstruction(unsigned int line);

	int GetInt(unsigned int line);

	std::string GetString(unsigned int line);

	char GetChar(unsigned int line);

	bool GetBool(unsigned int line);

	unsigned int GetInstrCount();

	std::vector<char> GetData();
	std::vector<char> GetInstr();
	
private:
	const static unsigned int MAX_BYTE_SIZE = 512;
	const static unsigned int MAX_INSTR_SIZE = 128;

	unsigned char data[MAX_BYTE_SIZE];
	unsigned char instr[MAX_INSTR_SIZE];
	unsigned int dataSize;
	unsigned int instrSize;
};

