#pragma once

#include <vector>

class Importer
{
public:
	Importer();
	Importer(std::vector<char> data);
	~Importer();

	char ReadChar();
	unsigned int ReadUint8();
	int ReadInt8();
	unsigned int ReadUint16();
	int ReadInt16();
	float ReadFloat();
	std::string ReadString();
	bool ReadBool();

private:
	std::vector<char> data;
	int index;
};

