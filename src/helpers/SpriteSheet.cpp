#include "stdafx.h"
#include "SpriteSheet.h"


SpriteSheet::SpriteSheet()
{
}

SpriteSheet::SpriteSheet(Texture texture, int spriteWidth, int spriteHeight) {
	this->texture = texture;
	this->spriteWidth = spriteWidth;
	this->spriteHeight = spriteHeight;
	this->sheetWidth = this->texture.GetWidth();
	this->sheetHeight = this->texture.GetHeight();
	this->rowSprites = this->sheetWidth / this->spriteWidth;
	this->colSprites = this->sheetHeight / this->spriteHeight;
}

SpriteSheet::SpriteSheet(Texture texture) {
	this->texture = texture;
	this->spriteWidth = this->texture.GetWidth();
	this->spriteHeight = this->texture.GetHeight();
	this->sheetWidth = this->texture.GetWidth();
	this->sheetHeight = this->texture.GetHeight();
	this->rowSprites = 1;
	this->colSprites = 1;
}


SpriteSheet::~SpriteSheet()
{
}

glm::vec4 SpriteSheet::GetTexCoords(int index) {
	float x1, x2, y1, y2;
	int x, y;
	x = index % this->rowSprites;
	y = (index - x) / this->rowSprites;
	x2 = 1 - (1.0f / this->rowSprites) * (x);
	x1 = 1 - (1.0f / this->rowSprites) * (x + 1);
	y1 = (1.0f / this->colSprites) * (y + 1);
	y2 = (1.0f / this->colSprites) * (y);

	return glm::vec4(x1, x2, y1, y2);
}

Texture SpriteSheet::GetTexture() {
	return this->texture;
}

SpriteSheet::Sprite SpriteSheet::GetSprite(int index) {
	glm::vec4 texCoords = this->GetTexCoords(index);
	return Sprite(glm::vec2(this->spriteWidth, this->spriteHeight), glm::vec2(texCoords.x, texCoords.z), glm::vec2(texCoords.y, texCoords.w), this->texture);
}
