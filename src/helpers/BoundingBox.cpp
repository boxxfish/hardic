#include "stdafx.h"
#include "BoundingBox.h"


BoundingBox::BoundingBox()
{
}

BoundingBox::BoundingBox(glm::vec2 position, glm::vec2 size)
	:position(position), size(size)
{

}


BoundingBox::~BoundingBox()
{
}

bool BoundingBox::ContainsPoint(glm::vec2 point) {
	if(std::abs(point.x - this->position.x) > (this->size.x / 2))
		return false;
	if (std::abs(point.y - this->position.y) > (this->size.y / 2))
		return false;
	return true;
}
