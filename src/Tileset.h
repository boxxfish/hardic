#pragma once

#include <vector>

#include "Importer.h"
#include "Model.h"
#include "SmshImporter.h"

class Tileset
{
public:
	Tileset();
	Tileset(std::vector<char> data, Resources* resources);
	~Tileset();

	std::vector<Model> tiles;
};

