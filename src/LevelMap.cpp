#include "LevelMap.h"
#include "stdafx.h"
#include "LevelMap.h"

LevelMap::LevelMap()
{
}

LevelMap::LevelMap(std::vector<char> data, Resources* resources) {
	Importer importer(data);

	//Header
	if (!(importer.ReadChar() == 'A' && importer.ReadChar() == 'L' && importer.ReadChar() == 'V' && importer.ReadChar() == 'L')) {
		std::cout << "Error: File is not a valid level map file.\n";
		return;
	}

	//Size
	this->width = importer.ReadUint8();
	this->height = importer.ReadUint8();

	//Tileset
	std::string tilesetName = importer.ReadString();
	this->tileset = Tileset(resources->GetResNode("tilesets/" + tilesetName + ".dat")->data.data, resources);

	this->numTileLayers = importer.ReadUint8();
	//Tile Layers
	for (int i = 0; i < numTileLayers; i++) {
		this->tileLayer.push_back(std::vector<std::vector<unsigned int>>());
		for (int y = 0; y < this->height; y++) {
			this->tileLayer[i].push_back(std::vector<unsigned int>());
			for (int x = 0; x < this->width; x++) {
				this->tileLayer[i][y].push_back(importer.ReadUint8());
			}
		}
	}

	//Object Layer
	for (int y = 0; y < this->height; y++) {
		this->objectLayer.push_back(std::vector<unsigned int>());
		for (int x = 0; x < this->width; x++) {
			this->objectLayer[y].push_back(importer.ReadUint8());
		}
	}
}


LevelMap::~LevelMap()
{
}
