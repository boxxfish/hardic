# README #

### What is this repository for? ###

* This is the repository for the game codenamed "Hardic"

### Dependencies ###

* GLEW
* GLFW
* GLM
* PortAudio
* ZLib
* LibPNG

### Contribution guidelines ###

* Ask before messing with someone else's code
* When you contribute, pick a task from the Trello board, assign yourself to it, then create a branch that incorporates the feature

### Who do I talk to? ###

* Ben or Marcello