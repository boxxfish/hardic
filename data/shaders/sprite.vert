#version 330

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 texCoords; 

uniform mat4 projMat;
uniform mat4 viewMat;
uniform mat4 modelMat;

out vec2 TexCoords;
out vec3 Position;

void main() {
	gl_Position = projMat * viewMat * modelMat * vec4(position, 1);
	Position = vec3(modelMat * vec4(position, 1));
	TexCoords = texCoords;
}