#version 330

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 texCoords; 

uniform mat4 projMat;
uniform mat4 viewMat;

uniform vec3 center;
uniform float size;

out vec2 TexCoords;

void main() {
	mat4 inverseView = inverse(viewMat);
	vec3 camUp = vec3(inverseView * vec4(0, 1, 0, 1));
	vec3 camRight = vec3(inverseView * vec4(1, 0, 0, 1));
	vec3 newPos = center + camRight * position.x * 0.01f + camUp * position.y * 0.01f;
	gl_Position = projMat * vec4(newPos, 1);
	TexCoords = texCoords;
}