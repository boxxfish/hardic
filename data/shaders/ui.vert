#version 330

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 texCoords; 

uniform mat4 projMat;
uniform mat4 modelMat;

out vec2 TexCoords;

void main() {
	gl_Position = projMat *  modelMat * vec4(position, 1);
	TexCoords = texCoords;
}