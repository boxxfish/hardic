#version 330

uniform sampler2D mainTex;

uniform vec3 dirLight;
uniform vec3 dirLightColor;
uniform float dirLightPower;

#define NUM_PT_LIGHTS 4
uniform vec3 ptLightPos[NUM_PT_LIGHTS];
uniform vec3 ptLightColor[NUM_PT_LIGHTS];
uniform float ptLightPower[NUM_PT_LIGHTS];

uniform vec3 camPos;

in vec3 Position;
in vec2 TexCoords;
in vec3 Normal;

out vec4 color;


float CalcSpecular(vec3 lightDir, vec3 camDir);

float CalcDiffuse(vec3 lightDir);

void main() {
	//Texture color
	color = texture(mainTex, TexCoords);
	if(color.a < 0.05f)
		discard;
	
	//Camera direction
	vec3 camDir = normalize(camPos - Position);
	
	//Ambient
	float ambient = 0.1f;
	
	//Direction light
	float diffuse = CalcDiffuse(-dirLight) * dirLightPower;
	float specular = CalcSpecular(-dirLight, camDir) * dirLightPower;
	//color.xyz += dirLightColor * dirLightPower;
	
	//Point lights
	for(int i = 0; i < NUM_PT_LIGHTS; i++) {
		//Light properties
		vec3 lightDir = normalize(ptLightPos[i] - Position);
		float att = 1.0f / (1.0f + 1.5f * length(ptLightPos[i] - Position) + 1.0f * length(ptLightPos[i] - Position) * length(ptLightPos[i] - Position));
		
		//Diffuse
		diffuse += CalcDiffuse(lightDir) * att * ptLightPower[i];
	
		//Specular
		specular += CalcSpecular(lightDir, camDir) * att * ptLightPower[i];
		
		//Color
		//color.xyz += ptLightPower[i];ptLightColor[i] * att * ptLightPower[i];
	}
	
	//Combine
	color.xyz *= clamp(0, 1, ambient + diffuse + specular);
}

float CalcDiffuse(vec3 lightDir) {
	return clamp(dot(lightDir, Normal), 0, 1);
}

float CalcSpecular(vec3 lightDir, vec3 camDir) {
	vec3 halfVec = normalize(camDir + lightDir);
	float spec = 32.0f;
	return clamp(0, 1, pow(dot(halfVec, Normal), spec));
}