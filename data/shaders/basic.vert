#version 330

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 texCoords; 
layout(location = 2) in vec3 normal; 

uniform mat4 projMat;
uniform mat4 viewMat;
uniform mat4 modelMat;

out vec3 Position;
out vec2 TexCoords;
out vec3 Normal;

void main() {
	gl_Position = projMat * viewMat * modelMat * vec4(position, 1);
	Position = vec3(modelMat * vec4(position, 1));
	TexCoords = 1 - texCoords;
	Normal = vec3(inverse(transpose(modelMat))  * vec4(normal, 0));
}