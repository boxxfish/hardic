#version 330

uniform vec4 tint;
uniform sampler2D sprite;

uniform vec3 dirLight;
uniform vec3 dirLightColor;
uniform float dirLightPower;

#define NUM_PT_LIGHTS 4
uniform vec3 ptLightPos[NUM_PT_LIGHTS];
uniform vec3 ptLightColor[NUM_PT_LIGHTS];
uniform float ptLightPower[NUM_PT_LIGHTS];

in vec3 Position;
in vec2 TexCoords;

out vec4 color;

void main() {
	//Texture color
	color = texture(sprite, TexCoords);
	if(color.a < 0.01f)
		discard;
	
	//Ambient
	float ambient = 0.1f;
	
	//Direction light
	float diffuse = dirLightPower;
	//color.xyz += dirLightColor * dirLightPower;
	
	//Point lights
	for(int i = 0; i < NUM_PT_LIGHTS; i++) {
		//Light properties
		float att = 1.0f / (1.0f + 1.5f * length(ptLightPos[i] - Position) + 1.0f * length(ptLightPos[i] - Position) * length(ptLightPos[i] - Position));
		
		//Diffuse
		diffuse += att * ptLightPower[i];
		
		//Color
		//color.xyz += ptLightColor[i] * att * ptLightPower[i];
	}
	
	//Combine
	color.xyz *= clamp(0, 1, ambient + diffuse);
}