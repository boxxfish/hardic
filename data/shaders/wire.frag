#version 330

uniform vec4 tint;

out vec4 color;

void main() {
	color = tint;
	if(color.a < 0.01f)
		discard;
}