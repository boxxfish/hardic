#version 330

uniform vec4 tint;
uniform sampler2D sprite;

in vec2 TexCoords;

out vec4 color;

void main() {
	color = texture(sprite, TexCoords) * tint;
	if(color.a < 0.01f)
		discard;
}